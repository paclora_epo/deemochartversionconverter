﻿#include "ui_Class_AppWindow_MainWindow.h"
#include "Class_AppWindow_MainWindow.h"
#include "Function_AutoSelectLanguage.hpp"

#include <QApplication>
#include <QTranslator>
#include <QFont>
#include <QStandardItemModel>
#include <QStringListModel>
#include <QtConcurrent>


// 创建本地化翻译家对象
QTranslator* 本地化  = new QTranslator(qApp);

// 创建用于共享的UI指针
Ui::AppWindow_MainWindow* AppWindow_BaseUi_UiShare = nullptr;

// 创建用于控制文件的数据模型
QStandardItemModel* 文件列表 = new QStandardItemModel(qApp);

// 创建用于显示日志的数据模型
QStandardItemModel* 日志列表 = new QStandardItemModel(qApp);

int main(int argc, char* argv[])
{
	// 创建应用程序对象
	QApplication App(argc, argv);

	// 读取由"自动选择语言()"提供的语言文件
	bool 本地化读取状态 = 本地化->load(自动选择语言());
	qDebug() << "\"main()\"的本地化文件读取状态为" << 本地化读取状态 << "->" << 本地化->language();
	App.installTranslator(本地化);

	// 创建并设置字体族
	 QFont 全局字体设置;
	 全局字体设置.setFamily("Microsoft Yahei");
	 App.setFont(全局字体设置);

	// 构建UI并显示UI
	AppWindow_MainWindow AppWindow_BaseUi;
	AppWindow_BaseUi_UiShare = AppWindow_BaseUi.UiSelf; // 重指向.
	AppWindow_BaseUi.show();

	// 检查启动命令
	if (argc > 1) { // https://doc.qt.io/qt-5/qcoreapplication.html#arguments	https://doc.qt.io/qt-5/qtconcurrentrun.html#using-member-functions
	for (int i = 1; i < argc; i++) AppWindow_BaseUi.UiSelf->tvw_FilePanel->面板引入文件列表中介.append(QString(App.arguments().at(i)));
	QtConcurrent::run(&AppWindow_BaseUi,&AppWindow_MainWindow::面板引入文件,2); } // 面板引入文件的逻辑已达完善,因此没有专为命令启动设计实现.

	return App.exec();
}
