﻿#pragma execution_character_set("UTF-8")
#ifndef FUNCTION_AUTOSELECTLANGUAGE_HPP
#define FUNCTION_AUTOSELECTLANGUAGE_HPP

#include <QLocale>
#include <QDebug>


// - 自动检测并选择本地语言, 返回一个QString;
// - 当前支持的语言: zh_CN, zh_TW, en_US.
QString 自动选择语言()
{
	// 创建本地信息对象
	QLocale 本地信息;

	// 通过本地语言选择语言文件
	switch (本地信息.language()) {
	// https://doc.qt.io/qt-5/qlocale.html#Language-enum
	// https://doc.qt.io/qt-5/qlocale.html#Country-enum

		case QLocale::English:
			// 如果本地语言为English, 则选择US文件
			qDebug() << "当前本地语言为English";
			return QString(":/language/DeemoChartConverter_Qt_en_US.qm");
			break;

		case QLocale::Chinese:
			// 如果本地语言为Chinese, 还要进一步筛选
			switch (本地信息.country()) {

				case QLocale::HongKong: case QLocale::Macau: case QLocale::Taiwan:
					// 如果地区在港澳台其中之一, 选择TW文件
					qDebug() << "当前本地语言为繁中";
					return QString(":/language/DeemoChartConverter_Qt_zh_TW.qm");
					break;

				default:
					// 如果地区不在港澳台, 则选择CN文件
					qDebug() << "当前本地语言为简中";
					return QString(":/language/DeemoChartConverter_Qt_zh_CN.qm");
					break;
			}
			break;

		default:
			// 对于不提供翻译文件的语言, 默认选择CN文件
			qDebug() << "当前本地语言不在预期范围内";
			return QString(":/language/DeemoChartConverter_Qt_zh_CN.qm");
			break;
	}
	qDebug() << "函数\"自动选择语言\"没有正确返回";
}


#endif // FUNCTION_AUTOSELECTLANGUAGE_HPP
