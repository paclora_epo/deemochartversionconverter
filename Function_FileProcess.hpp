﻿#pragma execution_character_set("UTF-8")
#ifndef FUNCTION_FILEPROCESS_HPP
#define FUNCTION_FILEPROCESS_HPP

#include <set>
#include <QDir>
#include <QList>
#include <QFile>
#include <QMap>
#include <vector>
#include <fstream>
#include <QDebug>
#include <QFileDialog>
#include <QTranslator>
#include <QStandardItemModel>

#include "Class_AppWindow_MainWindow.h"
#include "Class_CustomComponent.h"
#include "Function_OutputLog.hpp"
#include "ExternLibRef/json.hpp"





// 来自Main.cpp的全局指针.该指针会在Main()中指向ui_Class_AppWindow_MainWindow.h的UiSelf所指.
//extern Ui::AppWindow_MainWindow* AppWindow_BaseUi_UiShare;

enum 转换状态 { 错误 = -1, 就绪 = 0, 完成 = 1, 瑕疵 = 2 };

QMap<int,QString> 格式版本{ {0,"--"}, {1,"1.0"}, {3,"3.0"}, {4,"4.0"} };

Q_DECLARE_METATYPE(nlohmann::json*) // https://doc.qt.io/qt-5/qmetatype.html#Q_DECLARE_METATYPE
Q_DECLARE_METATYPE(nlohmann::json)
Q_DECLARE_METATYPE(nlohmann::ordered_json*)
Q_DECLARE_METATYPE(nlohmann::ordered_json)
Q_DECLARE_METATYPE(QStandardItem*)



// 用于转换检测的全局变量.
// - 目标版本: 仅支持[ 0 / 1 / 3 / 4 ],对应预设版本.
namespace 转换参数 {int 目标版本{0}; bool 超界警告{false}, 重叠警告{false}, 过小警告{false}, 过早警告{false}, 离谱警告{false}, 跳过空值{false}, 保持路径{false}; }



// 让用户通过一个窗口选择多个文件,该函数不做检查.
// - 父控件: QFileDialog::getOpenFileNames的必要参数,请指认主窗口.
// ! 函数实现稍作更改: 因多线程问题,本函数不再返回QList<QString>,改为使用成员传递.
void 弹窗选择文件(QWidget* 父控件, TreeView_Custom*文件面板) {
	QList<QString> 文件列表 = QFileDialog::getOpenFileNames (父控件, QObject::tr("选择文件"), "./",
		QObject::tr("JavaScript对象简谱 (*.json);;文本文档 (*.txt);;所有文件 (*.*)"));
	文件面板->面板引入文件列表中介 = 文件列表; }



// 检查文件存在性,返回一个仅包含有效文件路径的QList<QString>.
// - 引入文件列表: 未经检查的本地文件路径集.
QList<QString> 引入多个文件(const QList<QString> &引入文件列表) {
	QList<QString> 有效入文件路径集; QFileInfo 文件信息; QDir 目录控制; 目录控制.setFilter(QDir::Files|QDir::NoSymLinks); // https://doc.qt.io/qt-5/qdir.html#Filter-enum
	foreach (QString i, 引入文件列表) { // 遍历检查.
		文件信息.setFile(i); // https://doc.qt.io/qt-5/qfileinfo.html#setFile
		if (!文件信息.exists()) { // 如果路径不存在.
			添加日志面板信息(问题,QString(QObject::tr("没有找到\"%1\"。")).arg(i)); }
		else { // 如果路径存在.
			if (文件信息.isFile()) { // 如果路径是文件则添加.
				有效入文件路径集.append(i); }
			else { // 如果路径不是文件,
				if (文件信息.isDir()) { // 而是一个文件夹,则需要进一步处理.	https://www.cnblogs.com/pangblog/p/3258347.html
					目录控制.setPath(i); QFileInfoList 子列表信息{目录控制.entryInfoList()}; // https://doc.qt.io/qt-5/qdir.html#entryInfoList
					foreach (QFileInfo o, 子列表信息) { // 遍历子文件夹并添加筛选后的文件.
						有效入文件路径集.append(o.absoluteFilePath()); /* https://doc.qt.io/qt-5/qfileinfo.html#absoluteFilePath*/ }}}}}
	return 有效入文件路径集; }



// 检查文件是否为有效的谱面文件,依据结果返回解析指针或空指针.
// - 文件列表: 须提供一个有效的文件路径.
nlohmann::json* 检查单个文件(QString 完整位置) {
	QFile 有效性(完整位置);
	if (!有效性.exists()) { // 如果文件不存在则返回nullptr.
		添加日志面板信息(问题,QString(QObject::tr("没有找到\"%1\"。")).arg(完整位置));
		return nullptr; }
	if (!有效性.open(QIODevice::ReadOnly | QIODevice::Text)) { // 如果文件无法打开则返回nullptr.
		添加日志面板信息(问题,QString(QObject::tr("无法打开\"%1\"。")).arg(完整位置));
		return nullptr; }
	有效性.close(); // QFile检查完毕,关闭文件.

	std::ifstream 临时; 临时.open(完整位置.toLocal8Bit().toStdString(),std::ios_base::in); // https://blog.csdn.net/qq_33148269/article/details/77685343
	if (!临时.is_open()) { // 如果文件无法打开则返回nullptr.
		添加日志面板信息(问题,QString(QObject::tr("无法打开\"%1\"。")).arg(完整位置));
		return nullptr; }

	nlohmann::json* 检测 = new nlohmann::json;
	try { *检测 = nlohmann::json::parse(临时); } catch (nlohmann::json::parse_error& ex) { // 如果解析不成功则返回nullptr.
		//添加日志面板信息(问题,QString(QObject::tr("无法解析\"%1\"，细节： %2。")).arg(完整位置).arg(ex.what()));
		添加日志面板信息(问题,QString(QObject::tr("无法解析\"%1\"。")).arg(完整位置));
		临时.clear(); 临时.close(); delete 检测; // 清空和关闭未能成功传递的流,释放不能作为返回值的指针.
		return nullptr; }
	临时.clear(); 临时.close(); // ifstream已完成传递,清空和关闭流.

	auto 查键_Notes = 检测->find("notes");
	if (查键_Notes == 检测->end()) { // 如果没有"notes"则说明不是谱面格式,返回nullptr.
		添加日志面板信息(问题,QString(QObject::tr("内容有误\"%1\"。")).arg(完整位置));
		delete 检测; // 释放不能作为返回值的指针.
		return nullptr; }

	return 检测; /* 流程无误的返回,此指针将会在后续函数中被保存至数据模型.*/ }



// 向数据模型添加条目.条目共6列(idx0~idx5),分别是[ 文件序号 / 文件名称 / 格式版本 / 转换状态 / 文件路径 / 数据指针].
// 条目前5列(idx0~idx4)以QString形式保存,第6列(idx5)以包裹nlohmann::json*的QVariant形式保存且不予列出,取值时需格外注意.
// - 视图控件: 请指认与数据模型绑定的视图控件.
// - 目标模型: 请指认程序唯一QStandardItemModel.
// - 完整位置: 须提供文件的完整路径.
// - 数据指针: 只能由函数nlohmann::json* 检查单个文件(QString 完整位置)返回,本函数不检查数据指针的有效性.
void 创建单行数据到模型(TreeView_Custom* 视图控件, QStandardItemModel* 目标模型, QString 完整位置, nlohmann::json* 数据指针) {
	unsigned int 当前行 = 目标模型->rowCount(); QFileInfo 文件信息(完整位置); // https://blog.csdn.net/zq9955/article/details/122571576
	QStandardItem* 文件序号T = new QStandardItem(QString::number(当前行+1));
	QStandardItem* 文件路径T = new QStandardItem(QString(文件信息.canonicalPath()));
	QStandardItem* 文件名称T = new QStandardItem(QString(文件信息.fileName()));
	QStandardItem* 转换状态T = new QStandardItem(QString(QObject::tr("就绪")));
	QStandardItem* 格式版本T = new QStandardItem(QString(格式版本[0]));
	QStandardItem* 数据指针T = new QStandardItem; 数据指针T->setData(QVariant::fromValue(数据指针)); // https://bbs.csdn.net/topics/390372579	https://doc.qt.io/qt-5/qvariant.html#fromValue
	QList<QStandardItem*> 行{文件序号T,文件名称T,格式版本T,转换状态T,文件路径T,数据指针T};
	目标模型->appendRow(行); // 添加整行到模型.
	目标模型->item(当前行,0)->setTextAlignment(Qt::AlignRight); // https://doc.qt.io/qt-5/qt.html#AlignmentFlag-enum
	目标模型->item(当前行,2)->setTextAlignment(Qt::AlignCenter);
	目标模型->item(当前行,3)->setTextAlignment(Qt::AlignCenter);
	QMetaObject::invokeMethod(视图控件,[=](){视图控件->hideColumn(5); }); /* 第6种数据处于第5列,该数据不予展示. 此处多线程调用为解决初次置入时不生效的情况,相关问题仍未查明.	https://doc.qt.io/qt-5/qtreeview.html#hideColumn*/
	QMetaObject::invokeMethod(视图控件,[=](){视图控件->scrollTo(目标模型->indexFromItem(目标模型->item(目标模型->rowCount()-1))); }); /* 多线程下的条目滚动.*/ }



// 将指定条目从数据模型中移除.
// - 目标模型: 请指认程序唯一QStandardItemModel.
// - 目标行: 目标行的索引.
void 移除单行条目(QStandardItemModel* 目标模型, unsigned int 目标行) {
	nlohmann::json* 数据指针 = 目标模型->item(目标行,5)->data().value<nlohmann::json*>(); // https://doc.qt.io/qt-5/qstandarditem.html#data	https://doc.qt.io/qt-5/qvariant.html#value
	//delete 数据指针; 目标模型->takeRow(目标行); //取出数据指针并释放,随即移除整行. ——此实现存在内存泄漏.
	QList<QStandardItem*> 行合集 = 目标模型->takeRow(目标行);
	foreach (QStandardItem* i, 行合集) delete i; delete 数据指针; }



// 将选中的条目从数据模型中移除.
// - 视图控件: 请指认与数据模型绑定的视图控件.
// - 目标模型: 请指认程序唯一QStandardItemModel.
// ! 此函数的删除功能调用"移除单行条目".
void 移除选中条目(TreeView_Custom* 视图控件, QStandardItemModel* 目标模型) {
	QModelIndexList 已选; unsigned int 总选 = 视图控件->selectedIndexs().count() / 5; // 可见列为5,以此算出可见行.
	for (unsigned int i = 0; i < 总选; i++) { // 按选择数循环.
		已选 = 视图控件->selectedIndexs(); // 由于模型在进行删除操作removeRow()时可能会对索引进行重排,故每次重新获取.
		foreach (QModelIndex o, 已选) { // 遍历已选,
			if (o.column() == 0) { // 当遍历到行时(此处视列为0的索引为行)进行移除操作,
				移除单行条目(目标模型,o.row());
				break; /* 然后立刻终止循环.此处的模型索引可能发生了改变.*/ }}}}



// 对数据模型的第1列(idx0)进行排序.
// - 目标模型: 请指认程序唯一QStandardItemModel.
void 刷新条目编号(QStandardItemModel* 目标模型) {
	unsigned int 总行 = 目标模型->rowCount();
	for (unsigned int i = 0; i < 总行; i++) {
		//delete 目标模型->item(i,0);
		//目标模型->setItem(i, 0, new QStandardItem(QString::number(i+1)));
		目标模型->item(i,0)->setText(QString::number(i+1)); // 不删除原前背景样式.
		目标模型->item(i,0)->setTextAlignment(Qt::AlignRight); }}



// 对文件版本进行快速的识别,最终返回对应的int.
// - 目标模型: 请指认程序唯一QStandardItemModel.
// - 目标行: 目标行的索引.
int 获取单个文件版本(QStandardItemModel* 目标模型, unsigned int 目标行) {
	nlohmann::json 数据指针 = *目标模型->item(目标行,5)->data().value<nlohmann::json*>();
	if (数据指针["notes"].size() > 0) { // "notes"中至少要有一组数据.
		if (数据指针["notes"][0].find("shift") != 数据指针["notes"][0].end()) return 3; // 如果有"shift",判别为3.0版本.
		else { // 如果没有"shift",判断"$id"的类型.
			if (数据指针["notes"][0].find("$id") != 数据指针["notes"][0].end()) { // 如果有"$id",继续判断.
				if (数据指针["notes"][0]["$id"].is_string()) return 1; // 如果"$id"是字符串,判别为1.0版本.
				if (数据指针["notes"][0]["$id"].is_number()) return 4; /* 如果"$id"是数字,判别为4.0版本.*/ }}}
	添加日志面板信息(问题,QString(QObject::tr("%1.\"%2\"：版本特征不明确。")).arg(目标模型->item(目标行,0)->text(), 目标模型->item(目标行,1)->text()));
	return 0; /* 如果没有"$id"或"$id"类型不符,则视文件有误.*/ }



// 刷新数据模型的第三列(idx2).
// - 目标模型: 请指认程序唯一QStandardItemModel.
// - 目标行: 目标行的索引.
// - 目标版本: 仅支持[ 0 / 1 / 3 / 4 ],对应预设版本.
void 刷新条目版本(QStandardItemModel* 目标模型, unsigned int 目标行, int 目标版本) {
	delete 目标模型->item(目标行,2);
	目标模型->setItem(目标行, 2, new QStandardItem(QString(格式版本[目标版本])));
	目标模型->item(目标行,2)->setTextAlignment(Qt::AlignCenter); }



// 刷新数据模型的第四列(idx3),其中包含状态上色.
// - 目标模型: 请指认程序唯一QStandardItemModel.
// - 目标行: 目标行的索引.
// - 目标状态: 枚举[错误 || 就绪 || 完成 || 瑕疵],分别对应[错误(红色) / 就绪(原色) / 完成(绿色) / 完成(黄色)].
void 刷新转换状态(TreeView_Custom* 视图控件, QStandardItemModel* 目标模型, unsigned int 目标行, int 目标状态) {
	delete 目标模型->item(目标行,3);
	switch (目标状态) {
		case 错误:目标模型->setItem(目标行, 3, new QStandardItem(QString(QObject::tr("错误")))); 目标模型->item(目标行,3)->setForeground(QBrush(QColor(221,83,103))); 目标模型->item(目标行,0)->setBackground(QBrush(QColor(221,83,103),QBitmap(":/Image/自左向右渐变网点.png"))); break;
		case 就绪:目标模型->item(目标行,3)->setForeground(QBrush()); break;
		case 完成:目标模型->setItem(目标行, 3, new QStandardItem(QString(QObject::tr("完成")))); 目标模型->item(目标行,3)->setForeground(QBrush(QColor(143,181,65))); 目标模型->item(目标行,0)->setBackground(QBrush(QColor(143,181,65),QBitmap(":/Image/自左向右渐变网点.png"))); break;
		case 瑕疵:目标模型->setItem(目标行, 3, new QStandardItem(QString(QObject::tr("完成")))); 目标模型->item(目标行,3)->setForeground(QBrush(QColor(222,196,78))); 目标模型->item(目标行,0)->setBackground(QBrush(QColor(222,196,78),QBitmap(":/Image/自左向右渐变网点.png"))); break; }
	目标模型->item(目标行,3)->setTextAlignment(Qt::AlignCenter);
	QMetaObject::invokeMethod(视图控件,[=](){视图控件->scrollTo(目标模型->indexFromItem(目标模型->item(目标行))); }); }



// 附加检查组.函数组无法完全检查Key值有效性,须在文件引入流程后执行.
// ~ "pos","_time","w","d"在1.0规标中被视为不强制存在的浮点值,而其余值各有其读取时的缺省值.

void 检查进程_超界(const nlohmann::json &数据指针,const unsigned int &行标号, const QString &文件名, unsigned int &警告统计) { // "pos"的范围.
	unsigned int 元素数量 = 数据指针["notes"].size(); // "notes"总量.
	for (unsigned int i = 0; i < 元素数量; i++) { // 遍历"notes".
		if (数据指针["notes"][i].find("pos") != 数据指针["notes"][i].end()) { // 如果"pos"存在.("pos"可被允许不存在,且缺省值0.0必定合规.)
			if (!(数据指针["notes"][i]["pos"] >= -2 && 数据指针["notes"][i]["pos"] <= 2)) { // 如果检查出"pos"超界.
				添加日志面板信息(警告,QString(QObject::tr("%1.\"%2\"：Note位置超出玩家点击范围：")).arg(行标号).arg(文件名).append("\"notes\"(%1)->\"pos\"。").arg(i+1)); ++警告统计; }}}}

void 检查进程_重叠(const nlohmann::json &数据指针,const unsigned int &行标号, const QString &文件名, unsigned int &警告统计) { // "pos"及"_time"的距离.
	unsigned int 元素数量 = 数据指针["notes"].size(); // "notes"总量.
	std::vector<float> 位置循环真值, 时间循环真值; // 循环时的实际对比值,为避免反复检查存在性及后续赋值.
	for (unsigned int i = 0; i < 元素数量; i++) { // 填充容器,不存在则补为0.
		if (数据指针["notes"][i].find("pos") == 数据指针["notes"][i].end()) {位置循环真值.push_back(0.0); } else {位置循环真值.push_back(数据指针["notes"][i]["pos"].get<float>()); }
		if (数据指针["notes"][i].find("_time") == 数据指针["notes"][i].end()) {时间循环真值.push_back(0.0); } else {时间循环真值.push_back(数据指针["notes"][i]["_time"].get<float>()); }}
	for (unsigned int i = 0; i < 元素数量; i++) { // 主值遍历.
		if (!(位置循环真值[i] >= -2 && 位置循环真值[i] <= 2)) continue; // 如果Note超界则跳过.
		for (unsigned int o = i + 1; o < 元素数量; o++) { // 次值遍历.
			if (!(位置循环真值[o] >= -2 && 位置循环真值[o] <= 2)) continue; // 如果Note超界则跳过.对于琴键谱面而言此种情况较为频繁.
			if(fabs(时间循环真值[i] - 时间循环真值[o]) <= 0.012) { // 优先检查时间,如果两者"_time"绝对差小于等于0.012.
				if(fabs(位置循环真值[i] - 位置循环真值[o]) <= 0.03) { // 其次检查位置,如果两者"pos"绝对差小于等于0.03,则可以判定为存疑.
					if((时间循环真值[i] == 时间循环真值[o]) && (位置循环真值[i] == 位置循环真值[o])) { // 如果两者完全相等,汇报重叠,否则汇报存疑.
					添加日志面板信息(警告,QString(QObject::tr("%1.\"%2\"：发现完全重叠的Note坐标：")).arg(行标号).arg(文件名).append("\"notes\"(%1)&(%2)->\"pos\"&\"_time\"。").arg(i+1).arg(o+1)); ++警告统计; } else {
					添加日志面板信息(警告,QString(QObject::tr("%1.\"%2\"：发现疑似重叠的Note坐标：")).arg(行标号).arg(文件名).append("\"notes\"(%1)&(%2)->\"pos\"&\"_time\"。").arg(i+1).arg(o+1)); ++警告统计; }}}}}}

void 检查进程_过小(const nlohmann::json &数据指针,const unsigned int &行标号, const QString &文件名, unsigned int &警告统计) { // "size"的范围.
	unsigned int 元素数量 = 数据指针["notes"].size(); // "notes"总量.
	for (unsigned int i = 0; i < 元素数量; i++) { // 遍历"notes".
		if (数据指针["notes"][i].find("size") != 数据指针["notes"][i].end()) { // 如果"size"存在.("size"可被允许不存在,且缺省值1.0必定合规.)
			if (!(数据指针["notes"][i]["size"] > 0)) { // 如果"size"键宽过小.
				添加日志面板信息(警告,QString(QObject::tr("%1.\"%2\"：Note宽度已窄至不可见：")).arg(行标号).arg(文件名).append("\"notes\"(%1)->\"size\"。").arg(i+1)); ++警告统计; }}}}

void 检查进程_过早(const nlohmann::json &数据指针,const unsigned int &行标号, const QString &文件名, unsigned int &警告统计) { // "_time"的范围.
	unsigned int 元素数量 = 数据指针["notes"].size(); // "notes"总量.
	for (unsigned int i = 0; i < 元素数量; i++) { // 遍历"notes".
		if (数据指针["notes"][i].find("_time") != 数据指针["notes"][i].end()) { // 如果"_time"存在.("_time"可被允许不存在,且缺省值0.0必定合规.)
			if (!(数据指针["notes"][i]["_time"] >= 0)) { // 如果"_time"时间过早.
				添加日志面板信息(警告,QString(QObject::tr("%1.\"%2\"：Note时间早于音频起点：")).arg(行标号).arg(文件名).append("\"notes\"(%1)->\"_time\"。").arg(i+1)); ++警告统计; }}}}

void 检查进程_非常(const nlohmann::json &数据指针,const unsigned int &行标号, const QString &文件名, unsigned int &警告统计) { // "w"/"d"/"p"/"v"的范围.
	unsigned int 元素数量 = 数据指针["notes"].size(); // "notes"总量.
	for (unsigned int i = 0; i < 元素数量; i++) { // 遍历"notes".
		if (数据指针["notes"][i].find("sounds") != 数据指针["notes"][i].end()) { // 如果有"sounds".
			unsigned int 音效数量 = 数据指针["notes"][i]["sounds"].size(); // "sounds"总量.
			for (unsigned int o = 0; o < 音效数量; o++) { // 遍历"sounds".
				if (数据指针["notes"][i]["sounds"][o].find("w") != 数据指针["notes"][i]["sounds"][o].end()) { // 如果"w"存在.("w"可被允许不存在,且缺省值0.0必定合规.)
					if (!(数据指针["notes"][i]["sounds"][o]["w"] >= 0)) { // 如果"w"延迟过低.
						添加日志面板信息(警告,QString(QObject::tr("%1.\"%2\"：Note音效延迟已短于无延迟：")).arg(行标号).arg(文件名).append("\"notes\"(%1)->\"sounds\"(%2)->\"w\"。").arg(i+1).arg(o+1)); ++警告统计; }}
				if (数据指针["notes"][i]["sounds"][o].find("d") != 数据指针["notes"][i]["sounds"][o].end()) { // 如果"d"存在.("d"可被允许不存在,且缺省值0.0不会引发问题.)
					if (数据指针["notes"][i]["sounds"][o]["d"] < 0) { // 如果"d"时长过短.
						添加日志面板信息(警告,QString(QObject::tr("%1.\"%2\"：Note音效时长已短于不持续：")).arg(行标号).arg(文件名).append("\"notes\"(%1)->\"sounds\"(%2)->\"d\"。").arg(i+1).arg(o+1)); ++警告统计; }
					if (数据指针["notes"][i]["sounds"][o]["d"] == 0) {
						添加日志面板信息(警告,QString(QObject::tr("%1.\"%2\"：Note音效时长同等于不持续：")).arg(行标号).arg(文件名).append("\"notes\"(%1)->\"sounds\"(%2)->\"d\"。").arg(i+1).arg(o+1)); ++警告统计; }}
				else { // 如果"d"不存在,则将其视为缺省值与等零判别同等警告.
					添加日志面板信息(警告,QString(QObject::tr("%1.\"%2\"：Note音效时长同等于不持续：")).arg(行标号).arg(文件名).append("\"notes\"(%1)->\"sounds\"(%2)->\"d\"。").arg(i+1).arg(o+1)); ++警告统计; }
				if (!(数据指针["notes"][i]["sounds"][o]["p"] >= 0 && 数据指针["notes"][i]["sounds"][o]["p"] <=127)) { // 如果"p"音高超界.
					添加日志面板信息(警告,QString(QObject::tr("%1.\"%2\"：Note音效音高超出有效范围(0~127)：")).arg(行标号).arg(文件名).append("\"notes\"(%1)->\"sounds\"(%2)->\"p\"。").arg(i+1).arg(o+1)); ++警告统计; }
				if (!(数据指针["notes"][i]["sounds"][o]["v"] >= 0 && 数据指针["notes"][i]["sounds"][o]["v"] <=127)) { // 如果"v"音量超界.
					添加日志面板信息(警告,QString(QObject::tr("%1.\"%2\"：Note音效音量超出有效范围(0~127)：")).arg(行标号).arg(文件名).append("\"notes\"(%1)->\"sounds\"(%2)->\"v\"。").arg(i+1).arg(o+1)); ++警告统计; }}}}}



// 可转换单个文件.函数仅可转换为1.0和3.0格式,附加错误终止和必要警告.
// 若文件无误,返回一个装有nlohmann::ordered_json*的QVariant;否则返回nullptr.
// - 数据指针: 数据需指向文件的直接解析,通常来自唯一数据模型的第6列(inx5).
// - 行标号: 用于输出日志,通常来自唯一数据模型的第1列(inx0).
// - 文件名: 用于输出日志,通常来自唯一数据模型的第2列(inx1).
// - 警告统计: 外部计数用途,函数会随时更改此引用的实值.
// - 错误统计: 外部计数用途,函数会随时更改此引用的实值.
// ! 不推荐该函数直接访问UI控件,如有需要须额外实现非异步中介静态函数(可能会引发异线程阻塞问题).
// ! 该函数会访问命名空间"转换参数".
// * 如果您有意阅读此段代码,请先将其格式化为您喜欢或习惯的排列格式.
// ~ 实现更改: 20220504.1604: 因json库的间接访问接口不完善,故替换函数内部所有间接调用(->at() ->push_back() ).
// ~ 实现更改: 20220505.2208: 为适配古早格式,取消了对"pos","_time","w","d"的不存在时报错,改为静默补齐缺省.
// ~ 实现更改: 20220508.1512: 将4.0格式拆分为独立函数,使其能够排序为官方规标.
// ~ 实现更改: 20220604.0758: 为完善浮点缺省分配,取消了对"size"的不存在时报错,改为警告补齐缺省.
QVariant* 转换进程_定排1(const nlohmann::json &数据指针, const unsigned int &行标号, const QString &文件名, unsigned int &警告统计, unsigned int &错误统计) {
	nlohmann::ordered_json 固位容器;
	if (数据指针.find("speed") == 数据指针.end()) { // 找不到"speed".
		switch (转换参数::目标版本) { // 警告并添加"speed".
			case 0: 添加日志面板信息(警告,QString(QObject::tr("%1.\"%2\"：键值缺失：")).arg(行标号).arg(文件名).append("\"speed\"。")); ++警告统计; break;
			case 1:
			case 3: 添加日志面板信息(警告,QString(QObject::tr("%1.\"%2\"：键值缺失(已添加为默认值)：")).arg(行标号).arg(文件名).append("\"speed\"。")); ++警告统计;
				固位容器["speed"] = 0.0; break;
		}
	}
	else { // 检查到"speed".
		if (!(数据指针["speed"].is_number())) { // 如果"speed"不是数字类型.
			switch (转换参数::目标版本) { // 警告并替换"speed".
				case 0: 添加日志面板信息(警告,QString(QObject::tr("%1.\"%2\"：类型错误：")).arg(行标号).arg(文件名).append("\"speed\"。")); ++警告统计; break;
				case 1:
				case 3: 添加日志面板信息(警告,QString(QObject::tr("%1.\"%2\"：类型错误(已替换为默认值)：")).arg(行标号).arg(文件名).append("\"speed\"。")); ++警告统计;
					固位容器["speed"] = 0.0; break;
			}
		}
		else { // 如果"speed"是数字类型
			if (!(数据指针["speed"] >= 0)) { // "speed"过小.
				switch (转换参数::目标版本) { // 警告并替换"speed".
					case 0: 添加日志面板信息(警告,QString(QObject::tr("%1.\"%2\"：数值过小：")).arg(行标号).arg(文件名).append("\"speed\"。")); ++警告统计; break;
					case 1:
					case 3: 添加日志面板信息(警告,QString(QObject::tr("%1.\"%2\"：数值过小(已替换为默认值)：")).arg(行标号).arg(文件名).append("\"speed\"。")); ++警告统计;
						固位容器["speed"] = 0.0; break;
				}
			}
			else { // "speed"一切正常.
				固位容器["speed"] = 数据指针["speed"];
			}
		}
	}
	unsigned int 元素数量{数据指针["notes"].size()}; // "notes"总量.
	int 顺列偏移{1}; // "$id"的值与当前"notes"元素的序列值相差多少.
	std::set<int> 有效ID真值; // 保存"$id"的值以供"links"查找.
	if (元素数量 == 0) { // 如果"notes"没有元素.
		添加日志面板信息(警告,QString(QObject::tr("%1.\"%2\"：内容为空：")).arg(行标号).arg(文件名).append("\"notes\"(empty)。")); ++警告统计;
		固位容器["notes"] = { 0,0 }; 固位容器["notes"].clear();
	}
	for (unsigned int i{0}; i < 元素数量; i++) { // 遍历"notes".
		if (数据指针["notes"][i].find("$id") == 数据指针["notes"][i].end()) { // 找不到"$id".
			添加日志面板信息(问题,QString(QObject::tr("%1.\"%2\"：键值缺失：")).arg(行标号).arg(文件名).append("\"notes\"(%1)->\"$id\"。").arg(i+1)); ++错误统计;
			return nullptr;
		}
		else { // 检查到"$id".
			int ID类型{0}, 本位真值{0}, 上位真值{0};
			if (数据指针["notes"][i]["$id"].is_number_integer()) ID类型 = 1; // 如果"$id"是整数类型.
			else // 如果"$id"不是整数类型.
				if (数据指针["notes"][i]["$id"].is_string()) // 如果"$id"是字符类型.
					if (nlohmann::json::parse(std::string(数据指针["notes"][i]["$id"])).is_number_integer()) ID类型 = 2; //如果"$id"字符类型中是整数类型.
			switch (ID类型) { // "$id"的顺列检查.
				case 1: case 2: // "$id"是整数类型或套字符串整数类型.
					if (ID类型 == 1)
					{本位真值 = 数据指针["notes"][i]["$id"].get<int>(); } else
					{本位真值 = nlohmann::json::parse(std::string(数据指针["notes"][i]["$id"])).get<int>(); }
					if (本位真值 != (i + 顺列偏移)) { // 此组数据的"$id"与序列不匹配.
						if (i == 0) { // 如果i已是首位.
							顺列偏移 = 顺列偏移 + (本位真值 - 1); // 本位真值-1 通常就是本位与头的间隙.
							添加日志面板信息(警告,QString(QObject::tr("%1.\"%2\"：顺列偏移%3，\"$id\"为%4时开始：")).arg(行标号).arg(文件名).arg(本位真值-1).arg(本位真值).append("\"notes\"(%1)->\"$id\"。").arg(i+1)); ++警告统计;
						}
						else { // 如果i不是首位.
							if (ID类型 == 1)
							{上位真值 = 数据指针["notes"][i-1]["$id"].get<int>(); } else
							{上位真值 = nlohmann::json::parse(std::string(数据指针["notes"][i-1]["$id"])).get<int>(); }
							if (!(上位真值 < 本位真值)) { //  如果i的上一位不小于i.
								添加日志面板信息(问题,QString(QObject::tr("%1.\"%2\"：顺列混乱，\"$id\"为%4时开始：")).arg(行标号).arg(文件名).arg(本位真值).append("\"notes\"(%1)->\"$id\"。").arg(i+1)); ++错误统计;
								 return nullptr;
							}
							else { // i的上下位关系正常.
								顺列偏移 = 顺列偏移 + (本位真值 - 上位真值 - 1); // 本位真值-上位真值-1 通常就是本位与上位的间隙.
								添加日志面板信息(警告,QString(QObject::tr("%1.\"%2\"：顺列偏移%3(%4)，\"$id\"为%5时开始：")).arg(行标号).arg(文件名).arg(本位真值-上位真值-1).arg(顺列偏移-1).arg(本位真值).append("\"notes\"(%1)->\"$id\"。").arg(i+1)); ++警告统计;
							}
						}
					}
					else { // 此组数据的"$id"无误.
						if (ID类型 == 1) {
							固位容器["notes"][i]["$id"] = 数据指针["notes"][i]["$id"].dump(); } else { // 如果"$id"是整数类型,dump()为字符串.
							固位容器["notes"][i]["$id"] = 数据指针["notes"][i]["$id"]; } // 如果"$id"是套字符串整数类型,直接赋值.
						//变位容器["notes"][i]["$id"] = 本位真值;
						有效ID真值.insert(本位真值);
					}
					break;
				default: 添加日志面板信息(问题,QString(QObject::tr("%1.\"%2\"：类型错误：")).arg(行标号).arg(文件名).append("\"notes\"(%1)->\"$id\"。").arg(i+1)); ++错误统计;
					return nullptr;
			}
		}
		if (转换参数::目标版本 == 3) { // "type".
			固位容器["notes"][i]["type"] = 0;
		}
		if (数据指针["notes"][i].find("sounds") != 数据指针["notes"][i].end()) { // 存在"sounds"组.
			unsigned int 音效数量{数据指针["notes"][i]["sounds"].size()};
			for (unsigned int s{0}; s < 音效数量; s++) { // 遍历"sounds".
				if (数据指针["notes"][i]["sounds"][s].find("w") == 数据指针["notes"][i]["sounds"][s].end()) { // 如果"w"不存在.
					if (!转换参数::跳过空值) { // 如果用户要求不跳过.
						固位容器["notes"][i]["sounds"][s]["w"] = 0.0;
					}
				}
				else { // 如果"w"存在.
					if (!(数据指针["notes"][i]["sounds"][s]["w"].is_number())) { // 如果"w"不是数字类型.
						添加日志面板信息(问题,QString(QObject::tr("%1.\"%2\"：类型错误：")).arg(行标号).arg(文件名).append("\"notes\"(%1)->\"sounds\"(%2)->\"w\"。").arg(i+1).arg(s+1)); ++错误统计;
						return nullptr;
					}
					else { // 如果"w"是数字类型.
						if (!(转换参数::跳过空值 && 数据指针["notes"][i]["sounds"][s]["w"] == 0)) { // 如果用户要求不跳过或在要求跳过时"w"不为0.
							if (!(数据指针["notes"][i]["sounds"][s]["w"].is_number_float())) { // 如果"w"不是浮点类型.
								固位容器["notes"][i]["sounds"][s]["w"] = 数据指针["notes"][i]["sounds"][s]["w"].get<float>();
							}
							else { // "w"的类型无误.
								固位容器["notes"][i]["sounds"][s]["w"] = 数据指针["notes"][i]["sounds"][s]["w"];
							}
						}
					}
				}
				if (数据指针["notes"][i]["sounds"][s].find("d") == 数据指针["notes"][i]["sounds"][s].end()) { // 如果"d"不存在.
					if (!转换参数::跳过空值) { // 如果用户要求不跳过.
						固位容器["notes"][i]["sounds"][s]["d"] = 0.0;
					}
				}
				else { // 如果"d"存在.
					if (!(数据指针["notes"][i]["sounds"][s]["d"].is_number())) { // 如果"d"不是数字类型.
						添加日志面板信息(问题,QString(QObject::tr("%1.\"%2\"：类型错误：")).arg(行标号).arg(文件名).append("\"notes\"(%1)->\"sounds\"(%2)->\"d\"。").arg(i+1).arg(s+1)); ++错误统计;
						return nullptr;
					}
					else { // 如果"d"是数字类型.
						if (!(转换参数::跳过空值 && 数据指针["notes"][i]["sounds"][s]["d"] == 0)) { // 如果用户要求不跳过或在要求跳过时"d"不为0.
							if (!(数据指针["notes"][i]["sounds"][s]["d"].is_number_float())) { // 如果"d"不是浮点类型.
								固位容器["notes"][i]["sounds"][s]["d"] = 数据指针["notes"][i]["sounds"][s]["d"].get<float>();
							}
							else { // "d"的类型无误.
								固位容器["notes"][i]["sounds"][s]["d"] = 数据指针["notes"][i]["sounds"][s]["d"];
							}
						}
					}
				}
				if (数据指针["notes"][i]["sounds"][s].find("p") == 数据指针["notes"][i]["sounds"][s].end()) { // 如果"p"不存在.
					添加日志面板信息(问题,QString(QObject::tr("%1.\"%2\"：键值缺失：")).arg(行标号).arg(文件名).append("\"notes\"(%1)->\"sounds\"(%2)->\"p\"。").arg(i+1).arg(s+1)); ++错误统计;
					return nullptr;
				}
				else { // 如果"p"存在.
					if (!(数据指针["notes"][i]["sounds"][s]["p"].is_number_integer())) { // 如果"p"不是整数类型.
						添加日志面板信息(问题,QString(QObject::tr("%1.\"%2\"：类型错误：")).arg(行标号).arg(文件名).append("\"notes\"(%1)->\"sounds\"(%2)->\"p\"。").arg(i+1).arg(s+1)); ++错误统计;
						return nullptr;
					}
					else { // "p"的类型无误.
						固位容器["notes"][i]["sounds"][s]["p"] = 数据指针["notes"][i]["sounds"][s]["p"];
					}
				}
				if (数据指针["notes"][i]["sounds"][s].find("v") == 数据指针["notes"][i]["sounds"][s].end()) { // 如果"v"不存在.
					添加日志面板信息(问题,QString(QObject::tr("%1.\"%2\"：键值缺失：")).arg(行标号).arg(文件名).append("\"notes\"(%1)->\"sounds\"(%2)->\"v\"。").arg(i+1).arg(s+1)); ++错误统计;
					return nullptr;
				}
				else { // 如果"v"存在.
					if (!(数据指针["notes"][i]["sounds"][s]["v"].is_number_integer())) { // 如果"v"不是整数类型.
						添加日志面板信息(问题,QString(QObject::tr("%1.\"%2\"：类型错误：")).arg(行标号).arg(文件名).append("\"notes\"(%1)->\"sounds\"(%2)->\"v\"。").arg(i+1).arg(s+1)); ++错误统计;
						return nullptr;
					}
					else { // "v"的类型无误.
						固位容器["notes"][i]["sounds"][s]["v"] = 数据指针["notes"][i]["sounds"][s]["v"];
					}
				}
			}
		}
		if (数据指针["notes"][i].find("pos") == 数据指针["notes"][i].end()) { // 找不到"pos".
			//添加日志面板信息(问题,QString(QObject::tr("%1.\"%2\"：键值缺失：")).arg(行标号).arg(文件名).append("\"notes\"(%1)->\"pos\"。").arg(i+1)); ++错误统计;
			//return nullptr;
			if (!转换参数::跳过空值) { // 如果用户要求不跳过.
				固位容器["notes"][i]["pos"] = 0.0;
			}
		}
		else { // 检查到"pos".
			if (!(数据指针["notes"][i]["pos"].is_number())) { // 如果"pos"不是数字类型.
				添加日志面板信息(问题,QString(QObject::tr("%1.\"%2\"：类型错误：")).arg(行标号).arg(文件名).append("\"notes\"(%1)->\"pos\"。").arg(i+1)); ++错误统计;
				return nullptr;
			}
			else { // 如果"pos"是数字类型.
				if (!(转换参数::跳过空值 && 数据指针["notes"][i]["pos"] == 0)) { // 如果用户要求不跳过或在要求跳过时"pos"不为0.
					if (!(数据指针["notes"][i]["pos"].is_number_float())) { // 如果"pos"不是浮点类型.
						固位容器["notes"][i]["pos"] = 数据指针["notes"][i]["pos"].get<float>();
					}
					else { // "pos"的类型无误.
						固位容器["notes"][i]["pos"] = 数据指针["notes"][i]["pos"];
					}
				}
			}
		}
		if (数据指针["notes"][i].find("size") == 数据指针["notes"][i].end()) { // 找不到"size".
			添加日志面板信息(警告,QString(QObject::tr("%1.\"%2\"：键值缺失(已添加为默认值)：")).arg(行标号).arg(文件名).append("\"notes\"(%1)->\"size\"。").arg(i+1)); ++警告统计;
			固位容器["notes"][i]["size"] = 1.0;
		}
		else { // 检查到"size".
			if (!(数据指针["notes"][i]["size"].is_number())) { // 如果"size"不是数字类型.
				添加日志面板信息(问题,QString(QObject::tr("%1.\"%2\"：类型错误：")).arg(行标号).arg(文件名).append("\"notes\"(%1)->\"size\"。").arg(i+1)); ++错误统计;
				return nullptr;
			}
			else { // 如果"size"是数字类型.
				if (!(数据指针["notes"][i]["size"].is_number_float())) { // 如果"size"不是浮点类型.
					固位容器["notes"][i]["size"] = 数据指针["notes"][i]["size"].get<float>();
				}
				else { // "size"的类型无误.
					固位容器["notes"][i]["size"] = 数据指针["notes"][i]["size"];
				}
			}
		}
		if (数据指针["notes"][i].find("_time") == 数据指针["notes"][i].end()) { // 找不到"_time".
			//添加日志面板信息(问题,QString(QObject::tr("%1.\"%2\"：键值缺失：")).arg(行标号).arg(文件名).append("\"notes\"(%1)->\"_time\"。").arg(i+1)); ++错误统计;
			//return nullptr;
			if (!转换参数::跳过空值) { // 如果用户要求不跳过.
				固位容器["notes"][i]["_time"] = 0.0;
			}
		}
		else { // 检查到"_time".
			if (!(数据指针["notes"][i]["_time"].is_number())) { // 如果"_time"不是数字类型.
				添加日志面板信息(问题,QString(QObject::tr("%1.\"%2\"：类型错误：")).arg(行标号).arg(文件名).append("\"notes\"(%1)->\"_time\"。").arg(i+1)); ++错误统计;
				return nullptr;
			}
			else { // 如果"_time"是数字类型.
				if (!(转换参数::跳过空值 && 数据指针["notes"][i]["_time"] == 0)) { // 如果用户要求不跳过或在要求跳过时"_time"不为0.
					if (!(数据指针["notes"][i]["_time"].is_number_float())) { // 如果"_time"不是浮点类型.
						固位容器["notes"][i]["_time"] = 数据指针["notes"][i]["_time"].get<float>();
					}
					else { // "_time"的类型无误.
						固位容器["notes"][i]["_time"] = 数据指针["notes"][i]["_time"];
					}
				}
			}
		}
		if (转换参数::目标版本 == 3) { // "shift".
			固位容器["notes"][i]["shift"] = 0.0;
		}
		if (转换参数::目标版本 == 3) { // "time".
			固位容器["notes"][i]["time"] = 数据指针["notes"][i]["_time"];
		}
	}
	if (数据指针.find("links") == 数据指针.end()) { // 找不到"links".
		固位容器["links"] = { 0,0 }; 固位容器["links"].clear();
	}
	else { //检查到"links".
		unsigned int 连线数量{数据指针["links"].size()};
		if (连线数量 == 0) { // 如果"links"没有元素.
			固位容器["links"] = { 0,0 }; 固位容器["links"].clear();
		}
		for (unsigned int k{0}; k < 连线数量; k++) { // 遍历"links".
			if (数据指针["links"][k].find("notes") == 数据指针["links"][k].end()) { // 如果"links"的k成员不是"notes".
				添加日志面板信息(问题,QString(QObject::tr("%1.\"%2\"：内容错误：")).arg(行标号).arg(文件名).append("\"links\"(%1)->?。").arg(k+1)); ++错误统计;
				return nullptr;
			}
			else { // "links"的k成员是"notes".
				unsigned int 线段成员{数据指针["links"][k]["notes"].size()};
				for (unsigned int c{0}; c < 线段成员; c++) { // 遍历"notes"成员组.
					if (数据指针["links"][k]["notes"][c].find("$ref") == 数据指针["links"][k]["notes"][c].end()) { // 如果"notes"成员组内的c成员不是"$ref".
						添加日志面板信息(问题,QString(QObject::tr("%1.\"%2\"：内容错误：")).arg(行标号).arg(文件名).append("\"links\"(%1)->\"notes\"(%2)->?。").arg(k+1).arg(c+1)); ++错误统计;
						return nullptr;
					}
					else { // "notes"成员组内的c成员是"$ref".
						int REF类型{0}, REF真值{0};
						if (数据指针["links"][k]["notes"][c]["$ref"].is_number()) REF类型 = 1; // 如果"$ref"是数字类型.
						else // 如果"$ref"不是数字类型.
							if (数据指针["links"][k]["notes"][c]["$ref"].is_string()) // 如果"$ref"是字符类型.
								if (nlohmann::json::parse(std::string(数据指针["links"][k]["notes"][c]["$ref"])).is_number_integer()) REF类型 = 2; //如果"$ref"字符类型中是整数类型.
						switch (REF类型) { // "$ref"指向检查.
							case 1: case 2: // 如果"$ref"是整数类型或套字符串整数类型.
								if (REF类型 == 1)
								{REF真值 = 数据指针["links"][k]["notes"][c]["$ref"].get<int>(); } else
								{REF真值 = nlohmann::json::parse(std::string(数据指针["links"][k]["notes"][c]["$ref"])).get<int>(); }
								if (有效ID真值.find(REF真值) == 有效ID真值.end()) { // 如果"$ref"指向了不存在的"$id"
									添加日志面板信息(问题,QString(QObject::tr("%1.\"%2\"：标记虚指：")).arg(行标号).arg(文件名).append("\"links\"(%1)->\"notes\"(%2)->\"$ref\"~>\"$id\"(=%3?)。").arg(k+1).arg(c+1).arg(REF真值)); ++错误统计;
									return nullptr;
								}
								else { // "$ref"的指向无误.
									if (REF类型 == 1) {
										固位容器["links"][k]["notes"][c]["$ref"] = 数据指针["links"][k]["notes"][c]["$ref"].dump(); } else { // 如果"$id"是整数类型,dump()为字符串.
										固位容器["links"][k]["notes"][c]["$ref"] = 数据指针["links"][k]["notes"][c]["$ref"]; } // 如果"$id"是套字符串整数类型,直接赋值.
										//变位容器["links"][k]["notes"][c]["$ref"] = REF真值; break;
								}
								break;
							default: 添加日志面板信息(问题,QString(QObject::tr("%1.\"%2\"：类型错误：")).arg(行标号).arg(文件名).append("\"links\"(%1)->\"notes\"(%2)->\"$ref\"。").arg(k+1).arg(c+1)); ++错误统计;
								return nullptr;
						}
					}
				}
			}
		}
	}
	switch (转换参数::目标版本) { // 依据预设返回.
		case 0: return new QVariant;
		case 1:
		case 3: return new QVariant(QVariant::fromValue(new nlohmann::ordered_json(固位容器)));
	}
	return nullptr;
}

// 分离自"转换进程_定排1",仅可转换为4.0格式,其余功能保持一致,支持0模式.
QVariant* 转换进程_定排2(const nlohmann::json &数据指针, const unsigned int &行标号, const QString &文件名, unsigned int &警告统计, unsigned int &错误统计) {
	nlohmann::ordered_json 固位容器;
	unsigned int 元素数量{数据指针["notes"].size()}; // "notes"总量.
	int 顺列偏移{1}; // "$id"的值与当前"notes"元素的序列值相差多少.
	std::set<int> 有效ID真值; // 保存"$id"的值以供"links"查找.
	if (元素数量 == 0) { // 如果"notes"没有元素.
		添加日志面板信息(警告,QString(QObject::tr("%1.\"%2\"：内容为空：")).arg(行标号).arg(文件名).append("\"notes\"(empty)。")); ++警告统计;
		固位容器["notes"] = { 0,0 }; 固位容器["notes"].clear();
	}
	for (unsigned int i{0}; i < 元素数量; i++) { // 遍历"notes".

		if (数据指针["notes"][i].find("_time") == 数据指针["notes"][i].end()) { // 找不到"_time".
			//添加日志面板信息(问题,QString(QObject::tr("%1.\"%2\"：键值缺失：")).arg(行标号).arg(文件名).append("\"notes\"(%1)->\"_time\"。").arg(i+1)); ++错误统计;
			//return nullptr;
			if (!转换参数::跳过空值) { // 如果用户要求不跳过.
				固位容器["notes"][i]["_time"] = 0.0;
			}
		}
		else { // 检查到"_time".
			if (!(数据指针["notes"][i]["_time"].is_number())) { // 如果"_time"不是数字类型.
				添加日志面板信息(问题,QString(QObject::tr("%1.\"%2\"：类型错误：")).arg(行标号).arg(文件名).append("\"notes\"(%1)->\"_time\"。").arg(i+1)); ++错误统计;
				return nullptr;
			}
			else { // 如果"_time"是数字类型.
				if (!(转换参数::跳过空值 && 数据指针["notes"][i]["_time"] == 0)) { // 如果用户要求不跳过或在要求跳过时"_time"不为0.
					if (!(数据指针["notes"][i]["_time"].is_number_float())) { // 如果"_time"不是浮点类型.
						固位容器["notes"][i]["_time"] = 数据指针["notes"][i]["_time"].get<float>();
					}
					else { // "_time"的类型无误.
						固位容器["notes"][i]["_time"] = 数据指针["notes"][i]["_time"];
					}
				}
			}
		}
		if (数据指针["notes"][i].find("$id") == 数据指针["notes"][i].end()) { // 找不到"$id".
			添加日志面板信息(问题,QString(QObject::tr("%1.\"%2\"：键值缺失：")).arg(行标号).arg(文件名).append("\"notes\"(%1)->\"$id\"。").arg(i+1)); ++错误统计;
			return nullptr;
		}
		else { // 检查到"$id".
			int ID类型{0}, 本位真值{0}, 上位真值{0};
			if (数据指针["notes"][i]["$id"].is_number_integer()) ID类型 = 1; // 如果"$id"是整数类型.
			else // 如果"$id"不是整数类型.
				if (数据指针["notes"][i]["$id"].is_string()) // 如果"$id"是字符类型.
					if (nlohmann::json::parse(std::string(数据指针["notes"][i]["$id"])).is_number_integer()) ID类型 = 2; //如果"$id"字符类型中是整数类型.
			switch (ID类型) { // "$id"的顺列检查.
				case 1: case 2: // "$id"是整数类型或套字符串整数类型.
					if (ID类型 == 1)
					{本位真值 = 数据指针["notes"][i]["$id"].get<int>(); } else
					{本位真值 = nlohmann::json::parse(std::string(数据指针["notes"][i]["$id"])).get<int>(); }
					if (本位真值 != (i + 顺列偏移)) { // 此组数据的"$id"与序列不匹配.
						if (i == 0) { // 如果i已是首位.
							顺列偏移 = 顺列偏移 + (本位真值 - 1); // 本位真值-1 通常就是本位与头的间隙.
							添加日志面板信息(警告,QString(QObject::tr("%1.\"%2\"：顺列偏移%3，\"$id\"为%4时开始：")).arg(行标号).arg(文件名).arg(本位真值-1).arg(本位真值).append("\"notes\"(%1)->\"$id\"。").arg(i+1)); ++警告统计;
						}
						else { // 如果i不是首位.
							if (ID类型 == 1)
							{上位真值 = 数据指针["notes"][i-1]["$id"].get<int>(); } else
							{上位真值 = nlohmann::json::parse(std::string(数据指针["notes"][i-1]["$id"])).get<int>(); }
							if (!(上位真值 < 本位真值)) { //  如果i的上一位不小于i.
								添加日志面板信息(问题,QString(QObject::tr("%1.\"%2\"：顺列混乱，\"$id\"为%4时开始：")).arg(行标号).arg(文件名).arg(本位真值).append("\"notes\"(%1)->\"$id\"。").arg(i+1)); ++错误统计;
								 return nullptr;
							}
							else { // i的上下位关系正常.
								顺列偏移 = 顺列偏移 + (本位真值 - 上位真值 - 1); // 本位真值-上位真值-1 通常就是本位与上位的间隙.
								添加日志面板信息(警告,QString(QObject::tr("%1.\"%2\"：顺列偏移%3(%4)，\"$id\"为%5时开始：")).arg(行标号).arg(文件名).arg(本位真值-上位真值-1).arg(顺列偏移-1).arg(本位真值).append("\"notes\"(%1)->\"$id\"。").arg(i+1)); ++警告统计;
							}
						}
					}
					else { // 此组数据的"$id"无误.
						固位容器["notes"][i]["$id"] = 本位真值;
						有效ID真值.insert(本位真值);
					}
					break;
				default: 添加日志面板信息(问题,QString(QObject::tr("%1.\"%2\"：类型错误：")).arg(行标号).arg(文件名).append("\"notes\"(%1)->\"$id\"。").arg(i+1)); ++错误统计;
					return nullptr;
			}
		}
		if (数据指针["notes"][i].find("sounds") != 数据指针["notes"][i].end()) { // 存在"sounds"组.
			unsigned int 音效数量{数据指针["notes"][i]["sounds"].size()};
			for (unsigned int s{0}; s < 音效数量; s++) { // 遍历"sounds".
				if (数据指针["notes"][i]["sounds"][s].find("d") == 数据指针["notes"][i]["sounds"][s].end()) { // 如果"d"不存在.
					if (!转换参数::跳过空值) { // 如果用户要求不跳过.
						固位容器["notes"][i]["sounds"][s]["d"] = 0.0;
					}
				}
				else { // 如果"d"存在.
					if (!(数据指针["notes"][i]["sounds"][s]["d"].is_number())) { // 如果"d"不是数字类型.
						添加日志面板信息(问题,QString(QObject::tr("%1.\"%2\"：类型错误：")).arg(行标号).arg(文件名).append("\"notes\"(%1)->\"sounds\"(%2)->\"d\"。").arg(i+1).arg(s+1)); ++错误统计;
						return nullptr;
					}
					else { // 如果"d"是数字类型.
						if (!(转换参数::跳过空值 && 数据指针["notes"][i]["sounds"][s]["d"] == 0)) { // 如果用户要求不跳过或在要求跳过时"d"不为0.
							if (!(数据指针["notes"][i]["sounds"][s]["d"].is_number_float())) { // 如果"d"不是浮点类型.
								固位容器["notes"][i]["sounds"][s]["d"] = 数据指针["notes"][i]["sounds"][s]["d"].get<float>();
							}
							else { // "d"的类型无误.
								固位容器["notes"][i]["sounds"][s]["d"] = 数据指针["notes"][i]["sounds"][s]["d"];
							}
						}
					}
				}
				if (数据指针["notes"][i]["sounds"][s].find("p") == 数据指针["notes"][i]["sounds"][s].end()) { // 如果"p"不存在.
					添加日志面板信息(问题,QString(QObject::tr("%1.\"%2\"：键值缺失：")).arg(行标号).arg(文件名).append("\"notes\"(%1)->\"sounds\"(%2)->\"p\"。").arg(i+1).arg(s+1)); ++错误统计;
					return nullptr;
				}
				else { // 如果"p"存在.
					if (!(数据指针["notes"][i]["sounds"][s]["p"].is_number_integer())) { // 如果"p"不是整数类型.
						添加日志面板信息(问题,QString(QObject::tr("%1.\"%2\"：类型错误：")).arg(行标号).arg(文件名).append("\"notes\"(%1)->\"sounds\"(%2)->\"p\"。").arg(i+1).arg(s+1)); ++错误统计;
						return nullptr;
					}
					else { // "p"的类型无误.
						固位容器["notes"][i]["sounds"][s]["p"] = 数据指针["notes"][i]["sounds"][s]["p"];
					}
				}
				if (数据指针["notes"][i]["sounds"][s].find("v") == 数据指针["notes"][i]["sounds"][s].end()) { // 如果"v"不存在.
					添加日志面板信息(问题,QString(QObject::tr("%1.\"%2\"：键值缺失：")).arg(行标号).arg(文件名).append("\"notes\"(%1)->\"sounds\"(%2)->\"v\"。").arg(i+1).arg(s+1)); ++错误统计;
					return nullptr;
				}
				else { // 如果"v"存在.
					if (!(数据指针["notes"][i]["sounds"][s]["v"].is_number_integer())) { // 如果"v"不是整数类型.
						添加日志面板信息(问题,QString(QObject::tr("%1.\"%2\"：类型错误：")).arg(行标号).arg(文件名).append("\"notes\"(%1)->\"sounds\"(%2)->\"v\"。").arg(i+1).arg(s+1)); ++错误统计;
						return nullptr;
					}
					else { // "v"的类型无误.
						固位容器["notes"][i]["sounds"][s]["v"] = 数据指针["notes"][i]["sounds"][s]["v"];
					}
				}
				if (数据指针["notes"][i]["sounds"][s].find("w") == 数据指针["notes"][i]["sounds"][s].end()) { // 如果"w"不存在.
					if (!转换参数::跳过空值) { // 如果用户要求不跳过.
						固位容器["notes"][i]["sounds"][s]["w"] = 0.0;
					}
				}
				else { // 如果"w"存在.
					if (!(数据指针["notes"][i]["sounds"][s]["w"].is_number())) { // 如果"w"不是数字类型.
						添加日志面板信息(问题,QString(QObject::tr("%1.\"%2\"：类型错误：")).arg(行标号).arg(文件名).append("\"notes\"(%1)->\"sounds\"(%2)->\"w\"。").arg(i+1).arg(s+1)); ++错误统计;
						return nullptr;
					}
					else { // 如果"w"是数字类型.
						if (!(转换参数::跳过空值 && 数据指针["notes"][i]["sounds"][s]["w"] == 0)) { // 如果用户要求不跳过或在要求跳过时"w"不为0.
							if (!(数据指针["notes"][i]["sounds"][s]["w"].is_number_float())) { // 如果"w"不是浮点类型.
								固位容器["notes"][i]["sounds"][s]["w"] = 数据指针["notes"][i]["sounds"][s]["w"].get<float>();
							}
							else { // "w"的类型无误.
								固位容器["notes"][i]["sounds"][s]["w"] = 数据指针["notes"][i]["sounds"][s]["w"];
							}
						}
					}
				}
			}
		}
		if (数据指针["notes"][i].find("pos") == 数据指针["notes"][i].end()) { // 找不到"pos".
			//添加日志面板信息(问题,QString(QObject::tr("%1.\"%2\"：键值缺失：")).arg(行标号).arg(文件名).append("\"notes\"(%1)->\"pos\"。").arg(i+1)); ++错误统计;
			//return nullptr;
			if (!转换参数::跳过空值) { // 如果用户要求不跳过.
				固位容器["notes"][i]["pos"] = 0.0;
			}
		}
		else { // 检查到"pos".
			if (!(数据指针["notes"][i]["pos"].is_number())) { // 如果"pos"不是数字类型.
				添加日志面板信息(问题,QString(QObject::tr("%1.\"%2\"：类型错误：")).arg(行标号).arg(文件名).append("\"notes\"(%1)->\"pos\"。").arg(i+1)); ++错误统计;
				return nullptr;
			}
			else { // 如果"pos"是数字类型.
				if (!(转换参数::跳过空值 && 数据指针["notes"][i]["pos"] == 0)) { // 如果用户要求不跳过或在要求跳过时"pos"不为0.
					if (!(数据指针["notes"][i]["pos"].is_number_float())) { // 如果"pos"不是浮点类型.
						固位容器["notes"][i]["pos"] = 数据指针["notes"][i]["pos"].get<float>();
					}
					else { // "pos"的类型无误.
						固位容器["notes"][i]["pos"] = 数据指针["notes"][i]["pos"];
					}
				}
			}
		}
		if (数据指针["notes"][i].find("size") == 数据指针["notes"][i].end()) { // 找不到"size".
			添加日志面板信息(警告,QString(QObject::tr("%1.\"%2\"：键值缺失(已添加为默认值)：")).arg(行标号).arg(文件名).append("\"notes\"(%1)->\"size\"。").arg(i+1)); ++警告统计;
			固位容器["notes"][i]["size"] = 1.0;
		}
		else { // 检查到"size".
			if (!(数据指针["notes"][i]["size"].is_number())) { // 如果"size"不是数字类型.
				添加日志面板信息(问题,QString(QObject::tr("%1.\"%2\"：类型错误：")).arg(行标号).arg(文件名).append("\"notes\"(%1)->\"size\"。").arg(i+1)); ++错误统计;
				return nullptr;
			}
			else { // 如果"size"是数字类型.
				if (!(数据指针["notes"][i]["size"].is_number_float())) { // 如果"size"不是浮点类型.
					固位容器["notes"][i]["size"] = 数据指针["notes"][i]["size"].get<float>();
				}
				else { // "size"的类型无误.
					固位容器["notes"][i]["size"] = 数据指针["notes"][i]["size"];
				}
			}
		}
	}
	if (数据指针.find("links") == 数据指针.end()) { // 找不到"links".
		固位容器["links"] = { 0,0 }; 固位容器["links"].clear();
	}
	else { //检查到"links".
		unsigned int 连线数量{数据指针["links"].size()};
		if (连线数量 == 0) { // 如果"links"没有元素.
			固位容器["links"] = { 0,0 }; 固位容器["links"].clear();
		}
		for (unsigned int k{0}; k < 连线数量; k++) { // 遍历"links".
			if (数据指针["links"][k].find("notes") == 数据指针["links"][k].end()) { // 如果"links"的k成员不是"notes".
				添加日志面板信息(问题,QString(QObject::tr("%1.\"%2\"：内容错误：")).arg(行标号).arg(文件名).append("\"links\"(%1)->?。").arg(k+1)); ++错误统计;
				return nullptr;
			}
			else { // "links"的k成员是"notes".
				unsigned int 线段成员{数据指针["links"][k]["notes"].size()};
				for (unsigned int c{0}; c < 线段成员; c++) { // 遍历"notes"成员组.
					if (数据指针["links"][k]["notes"][c].find("$ref") == 数据指针["links"][k]["notes"][c].end()) { // 如果"notes"成员组内的c成员不是"$ref".
						添加日志面板信息(问题,QString(QObject::tr("%1.\"%2\"：内容错误：")).arg(行标号).arg(文件名).append("\"links\"(%1)->\"notes\"(%2)->?。").arg(k+1).arg(c+1)); ++错误统计;
						return nullptr;
					}
					else { // "notes"成员组内的c成员是"$ref".
						int REF类型{0}, REF真值{0};
						if (数据指针["links"][k]["notes"][c]["$ref"].is_number()) REF类型 = 1; // 如果"$ref"是数字类型.
						else // 如果"$ref"不是数字类型.
							if (数据指针["links"][k]["notes"][c]["$ref"].is_string()) // 如果"$ref"是字符类型.
								if (nlohmann::json::parse(std::string(数据指针["links"][k]["notes"][c]["$ref"])).is_number_integer()) REF类型 = 2; //如果"$ref"字符类型中是整数类型.
						switch (REF类型) { // "$ref"指向检查.
							case 1: case 2: // 如果"$ref"是整数类型或套字符串整数类型.
								if (REF类型 == 1)
								{REF真值 = 数据指针["links"][k]["notes"][c]["$ref"].get<int>(); } else
								{REF真值 = nlohmann::json::parse(std::string(数据指针["links"][k]["notes"][c]["$ref"])).get<int>(); }
								if (有效ID真值.find(REF真值) == 有效ID真值.end()) { // 如果"$ref"指向了不存在的"$id"
									添加日志面板信息(问题,QString(QObject::tr("%1.\"%2\"：标记虚指：")).arg(行标号).arg(文件名).append("\"links\"(%1)->\"notes\"(%2)->\"$ref\"~>\"$id\"(=%3?)。").arg(k+1).arg(c+1).arg(REF真值)); ++错误统计;
									return nullptr;
								}
								else { // "$ref"的指向无误.
									固位容器["links"][k]["notes"][c]["$ref"] = REF真值;
								}
								break;
							default: 添加日志面板信息(问题,QString(QObject::tr("%1.\"%2\"：类型错误：")).arg(行标号).arg(文件名).append("\"links\"(%1)->\"notes\"(%2)->\"$ref\"。").arg(k+1).arg(c+1)); ++错误统计;
								return nullptr;
						}
					}
				}
			}
		}
	}
	if (数据指针.find("speed") == 数据指针.end()) { // 找不到"speed".
		switch (转换参数::目标版本) { // 警告并添加"speed".
			case 0: 添加日志面板信息(警告,QString(QObject::tr("%1.\"%2\"：键值缺失：")).arg(行标号).arg(文件名).append("\"speed\"。")); ++警告统计; break;
			case 4: 添加日志面板信息(警告,QString(QObject::tr("%1.\"%2\"：键值缺失(已添加为默认值)：")).arg(行标号).arg(文件名).append("\"speed\"。")); ++警告统计;
				固位容器["speed"] = 0.0; break;
		}
	}
	else { // 检查到"speed".
		if (!(数据指针["speed"].is_number())) { // 如果"speed"不是数字类型.
			switch (转换参数::目标版本) { // 警告并替换"speed".
				case 0: 添加日志面板信息(警告,QString(QObject::tr("%1.\"%2\"：类型错误：")).arg(行标号).arg(文件名).append("\"speed\"。")); ++警告统计; break;
				case 4: 添加日志面板信息(警告,QString(QObject::tr("%1.\"%2\"：类型错误(已替换为默认值)：")).arg(行标号).arg(文件名).append("\"speed\"。")); ++警告统计;
					固位容器["speed"] = 0.0; break;
			}
		}
		else { // 如果"speed"是数字类型
			if (!(数据指针["speed"] >= 0)) { // "speed"过小.
				switch (转换参数::目标版本) { // 警告并替换"speed".
					case 0: 添加日志面板信息(警告,QString(QObject::tr("%1.\"%2\"：数值过小：")).arg(行标号).arg(文件名).append("\"speed\"。")); ++警告统计; break;
					case 4: 添加日志面板信息(警告,QString(QObject::tr("%1.\"%2\"：数值过小(已替换为默认值)：")).arg(行标号).arg(文件名).append("\"speed\"。")); ++警告统计;
						固位容器["speed"] = 0.0; break;
				}
			}
			else { // "speed"一切正常.
				固位容器["speed"] = 数据指针["speed"];
			}
		}
	}
	switch (转换参数::目标版本) { // 依据预设返回.
		case 0: return new QVariant;
		case 4: return new QVariant(QVariant::fromValue(new nlohmann::ordered_json(固位容器)));
	}
	return nullptr;
}




#endif  // FUNCTION_FILEPROCESS_HPP
