<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="en_US">
<context>
    <name>AppWindow_MainWindow</name>
    <message>
        <location filename="../Class_AppWindow_MainWindow.ui" line="32"/>
        <source>古树旋律谱面版本转换器</source>
        <translation>Deemo Chart Version Converter   -1.3.1.2205</translation>
    </message>
    <message>
        <location filename="../Class_AppWindow_MainWindow.ui" line="233"/>
        <source>目标版本</source>
        <translation>TargetFormat</translation>
    </message>
    <message>
        <location filename="../Class_AppWindow_MainWindow.ui" line="252"/>
        <source>1.0</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Class_AppWindow_MainWindow.ui" line="268"/>
        <source>3.0</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Class_AppWindow_MainWindow.ui" line="284"/>
        <source>4.0</source>
        <translation></translation>
    </message>
    <message>
        <location filename="../Class_AppWindow_MainWindow.ui" line="294"/>
        <source>仅检查</source>
        <translation>CheckOnly</translation>
    </message>
    <message>
        <location filename="../Class_AppWindow_MainWindow.ui" line="328"/>
        <source>附加检查</source>
        <translation>AdditionalOption</translation>
    </message>
    <message>
        <location filename="../Class_AppWindow_MainWindow.ui" line="341"/>
        <source>超界</source>
        <translation>BoundsOut</translation>
    </message>
    <message>
        <location filename="../Class_AppWindow_MainWindow.ui" line="351"/>
        <source>重叠</source>
        <translation>Overlap</translation>
    </message>
    <message>
        <location filename="../Class_AppWindow_MainWindow.ui" line="361"/>
        <source>过小</source>
        <translation>TooSmall</translation>
    </message>
    <message>
        <location filename="../Class_AppWindow_MainWindow.ui" line="371"/>
        <source>过早</source>
        <translation>TooEarly</translation>
    </message>
    <message>
        <location filename="../Class_AppWindow_MainWindow.ui" line="407"/>
        <source>音效非常</source>
        <translation>NonStandardSounds</translation>
    </message>
    <message>
        <location filename="../Class_AppWindow_MainWindow.ui" line="420"/>
        <source>跳过空值</source>
        <translation>SkipInvalidValues</translation>
    </message>
    <message>
        <location filename="../Class_AppWindow_MainWindow.ui" line="430"/>
        <source>保持路径</source>
        <translation>KeepPath</translation>
    </message>
    <message>
        <location filename="../Class_AppWindow_MainWindow.ui" line="453"/>
        <source>前缀|后缀</source>
        <translation>Prefix|Suffix</translation>
    </message>
    <message>
        <location filename="../Class_AppWindow_MainWindow.ui" line="510"/>
        <source>简体</source>
        <translation>ENG</translation>
    </message>
    <message>
        <location filename="../Class_AppWindow_MainWindow.ui" line="532"/>
        <source>检查谱面版本</source>
        <translation>GetChartsVersion</translation>
    </message>
    <message>
        <location filename="../Class_AppWindow_MainWindow.ui" line="548"/>
        <source>转换所有谱面</source>
        <translation>ConvertAllCharts</translation>
    </message>
    <message>
        <location filename="../Class_AppWindow_MainWindow.cpp" line="320"/>
        <source>语言切换——可在&quot;简体中文&quot;、&quot;正體中文&quot;和&quot;English&quot;中切换。当前语言为&quot;简体中文&quot;。</source>
        <translation>Language switch: Switchable between &quot;English&quot;, &quot;简体中文&quot; and &quot;正體中文&quot;. The current language is &quot;English&quot;.</translation>
    </message>
    <message>
        <location filename="../Class_AppWindow_MainWindow.cpp" line="323"/>
        <source>检查谱面版本——检查谱面的格式版本，结果将显示于文件名右侧。</source>
        <translation>Get charts version: Get format version of charts and the result will be displayed to right of file name.</translation>
    </message>
    <message>
        <location filename="../Class_AppWindow_MainWindow.cpp" line="326"/>
        <source>转换所有谱面——立刻按照所选参数转换所有谱面。</source>
        <translation>Convert all charts: Convert all charts according to the selected option.</translation>
    </message>
    <message>
        <location filename="../Class_AppWindow_MainWindow.cpp" line="329"/>
        <source>文件名前缀——经过转换的文件是以&quot;前缀原文件名后缀&quot;的形式的命名的，此处填写的内容将作为&quot;前缀&quot;。</source>
        <translation>File prefix: New files are named in the form PrefixFilenameSuffix.</translation>
    </message>
    <message>
        <location filename="../Class_AppWindow_MainWindow.cpp" line="332"/>
        <source>文件名后缀——经过转换的文件是以&quot;前缀原文件名后缀&quot;的形式的命名的，此处填写的内容将作为&quot;后缀&quot;。</source>
        <translation>File suffix: New files are named in the form PrefixFilenameSuffix.</translation>
    </message>
    <message>
        <location filename="../Class_AppWindow_MainWindow.cpp" line="335"/>
        <source>超出轨道边界——勾选此选项可令程序在检查到超出轨道范围的按键时发出警告(包括不可视按键)。</source>
        <translation>Notes outside the valid range: Issue warning when note is not in -2.0~2.0.</translation>
    </message>
    <message>
        <location filename="../Class_AppWindow_MainWindow.cpp" line="338"/>
        <source>坐标完全重叠——勾选此选项可令程序在检查到时间与位置几乎相同的可视按键时发出警告。</source>
        <translation>Coordinate overlap: Issue warning when position and time of note almost overlap.</translation>
    </message>
    <message>
        <location filename="../Class_AppWindow_MainWindow.cpp" line="341"/>
        <source>宽度过小——勾选此选项可令程序在检查到宽度不大于0的按键时发出警告。</source>
        <translation>Size is too small: Issue warning when size of note is not greater than 0.0.</translation>
    </message>
    <message>
        <location filename="../Class_AppWindow_MainWindow.cpp" line="344"/>
        <source>时间过早——勾选此选项可令程序在检查到时间小于0的按键时发出警告。</source>
        <translation>Time is too early: Issue warning when time of note is less than 0:00.</translation>
    </message>
    <message>
        <location filename="../Class_AppWindow_MainWindow.cpp" line="347"/>
        <source>音效非常——勾选此选项可令程序在检查到与预期数值范围不符的效果音属性时发出警告。</source>
        <translation>Non-standard sound: Issue warning when a property of sound is not conform to expected range.</translation>
    </message>
    <message>
        <location filename="../Class_AppWindow_MainWindow.cpp" line="350"/>
        <source>跳过空值——这是一个转换选项，它可令程序在转换时忽略部分空值。对&quot;1.0&quot;版本以外的预期转换使用此选项将导致文件不合规标。</source>
        <translation>Skip invalid values: This is a convert option, program will ignores some invalid values when converting.</translation>
    </message>
    <message>
        <location filename="../Class_AppWindow_MainWindow.cpp" line="353"/>
        <source>保持路径——这是一个转换选项，它可令新文件创建在与源文件相同的文件夹中。</source>
        <translation>Keep source path: This is a convert option, program will creates new files in the same folder as source files.</translation>
    </message>
    <message>
        <location filename="../Class_AppWindow_MainWindow.cpp" line="356"/>
        <source>仅检查——选择此项可令程序仅按照所选设置检查文件内容，而不会转换出新文件。</source>
        <translation>Check only: Check the files against selected option without converting to new files.</translation>
    </message>
    <message>
        <location filename="../Class_AppWindow_MainWindow.cpp" line="359"/>
        <source>转为第1.0版本——最早的谱面格式，也是可视化关卡编辑器平台中最通用的版本。</source>
        <translation>Convert to 1.0: The most compatibility of visual chart editor platforms.</translation>
    </message>
    <message>
        <location filename="../Class_AppWindow_MainWindow.cpp" line="362"/>
        <source>转为第3.0版本——于2.4版本出现的谱面格式，其内容包含部分无效信息，是现存格式中最臃肿的版本。</source>
        <translation>Convert to 3.0: The most bloated version of existing format.</translation>
    </message>
    <message>
        <location filename="../Class_AppWindow_MainWindow.cpp" line="365"/>
        <source>转为第4.0版本——于4.0版本出现的谱面格式，相对于旧版主要统一了数据类型，因此不再与先前版本兼容。</source>
        <translation>Convert to 4.0: No longer compatible with previous versions due to unification of data types.</translation>
    </message>
    <message>
        <location filename="../Class_AppWindow_MainWindow.cpp" line="368"/>
        <source>日志面板——绿色、黄色和红色背景的提示信息分别是流程、警告和错误类型的日志。使用鼠标可单选或多选条目，右键可呼出操作菜单。</source>
        <translation>Log panel: Prompts with green, yellow, and red backgrounds correspond to logs of process, warning, and error types. Right click can bring up menu.</translation>
    </message>
    <message>
        <location filename="../Class_AppWindow_MainWindow.cpp" line="371"/>
        <source>文件面板——展示文件的信息和状态。使用鼠标可单选或多选条目，右键可呼出操作菜单，在窗口内双击可选择并引入文件。</source>
        <translation>File panel: Show information and status of files. Right click can bring up the menu, double click inside the panel to select some files.</translation>
    </message>
    <message>
        <location filename="../Class_AppWindow_MainWindow.cpp" line="40"/>
        <location filename="../Class_AppWindow_MainWindow.cpp" line="206"/>
        <source>文件名</source>
        <translation>FileName</translation>
    </message>
    <message>
        <location filename="../Class_AppWindow_MainWindow.cpp" line="40"/>
        <location filename="../Class_AppWindow_MainWindow.cpp" line="206"/>
        <source>版本</source>
        <translation>Version</translation>
    </message>
    <message>
        <location filename="../Class_AppWindow_MainWindow.cpp" line="40"/>
        <location filename="../Class_AppWindow_MainWindow.cpp" line="206"/>
        <source>状态</source>
        <translation>State</translation>
    </message>
    <message>
        <location filename="../Class_AppWindow_MainWindow.cpp" line="40"/>
        <location filename="../Class_AppWindow_MainWindow.cpp" line="206"/>
        <source>路径</source>
        <translation>Path</translation>
    </message>
    <message>
        <location filename="../Class_AppWindow_MainWindow.cpp" line="65"/>
        <location filename="../Class_AppWindow_MainWindow.cpp" line="70"/>
        <source>复制全部条目</source>
        <translation>Copy All Logs</translation>
    </message>
    <message>
        <location filename="../Class_AppWindow_MainWindow.cpp" line="67"/>
        <location filename="../Class_AppWindow_MainWindow.cpp" line="73"/>
        <source>清除全部条目</source>
        <translation>Clear All Logs</translation>
    </message>
    <message>
        <location filename="../Class_AppWindow_MainWindow.cpp" line="69"/>
        <source>复制选中条目</source>
        <translation>Copy Selected Logs</translation>
    </message>
    <message>
        <location filename="../Class_AppWindow_MainWindow.cpp" line="72"/>
        <source>清除选中条目</source>
        <translation>Clear Selected Logs</translation>
    </message>
    <message>
        <location filename="../Class_AppWindow_MainWindow.cpp" line="111"/>
        <source>引入文件...</source>
        <translation>Import Files...</translation>
    </message>
    <message>
        <location filename="../Class_AppWindow_MainWindow.cpp" line="115"/>
        <location filename="../Class_AppWindow_MainWindow.cpp" line="119"/>
        <source>移除所有条目</source>
        <translation>Remove All Entries</translation>
    </message>
    <message>
        <location filename="../Class_AppWindow_MainWindow.cpp" line="118"/>
        <source>移除所选条目</source>
        <translation>Remove Selected Entries</translation>
    </message>
</context>
<context>
    <name>QFileDialog</name>
    <message>
        <location filename="../Class_AppWindow_MainWindow.cpp" line="249"/>
        <source>共引入%1个文件。</source>
        <translation>%1 files were imported.</translation>
    </message>
    <message>
        <location filename="../Class_AppWindow_MainWindow.cpp" line="313"/>
        <source>%1个警告，%2个错误。</source>
        <translation>%1 errors, %2 warnings.</translation>
    </message>
</context>
<context>
    <name>QObject</name>
    <message>
        <location filename="../Class_AppWindow_MainWindow.cpp" line="296"/>
        <source>%1.&quot;%2&quot;：写入文件时出错。</source>
        <translation>%1.&quot;%2&quot;: error writing to file.</translation>
    </message>
    <message>
        <location filename="../Function_FileProcess.hpp" line="208"/>
        <source>错误</source>
        <translation>Error</translation>
    </message>
    <message>
        <location filename="../Function_FileProcess.hpp" line="123"/>
        <source>就绪</source>
        <translation>Ready</translation>
    </message>
    <message>
        <location filename="../Function_FileProcess.hpp" line="210"/>
        <location filename="../Function_FileProcess.hpp" line="211"/>
        <source>完成</source>
        <translation>Complete</translation>
    </message>
    <message>
        <location filename="../Function_FileProcess.hpp" line="51"/>
        <source>选择文件</source>
        <translation>Select files</translation>
    </message>
    <message>
        <location filename="../Function_FileProcess.hpp" line="52"/>
        <source>JavaScript对象简谱 (*.json);;文本文档 (*.txt);;所有文件 (*.*)</source>
        <translation>JavaScript Object Notation (*.json);;Text Document (*.txt);;All Types (*.*)</translation>
    </message>
    <message>
        <location filename="../Function_FileProcess.hpp" line="64"/>
        <location filename="../Function_FileProcess.hpp" line="82"/>
        <source>没有找到&quot;%1&quot;。</source>
        <translation>&quot;%1&quot; not found.</translation>
    </message>
    <message>
        <location filename="../Function_FileProcess.hpp" line="85"/>
        <location filename="../Function_FileProcess.hpp" line="91"/>
        <source>无法打开&quot;%1&quot;。</source>
        <translation>&quot;%1&quot; cannot be opened.</translation>
    </message>
    <message>
        <location filename="../Function_FileProcess.hpp" line="97"/>
        <source>无法解析&quot;%1&quot;。</source>
        <translation>&quot;%1&quot; cannot be parsed.</translation>
    </message>
    <message>
        <location filename="../Function_FileProcess.hpp" line="104"/>
        <source>内容有误&quot;%1&quot;。</source>
        <translation>&quot;%1&quot; has wrong with content.</translation>
    </message>
    <message>
        <location filename="../Function_FileProcess.hpp" line="185"/>
        <source>%1.&quot;%2&quot;：版本特征不明确。</source>
        <translation>%1.&quot;%2&quot;: Version characteristics are ambiguous.</translation>
    </message>
    <message>
        <location filename="../Function_FileProcess.hpp" line="225"/>
        <source>%1.&quot;%2&quot;：Note位置超出玩家点击范围：</source>
        <translation>%1.&quot;%2&quot;: Note is positioned beyond the range of player can click: </translation>
    </message>
    <message>
        <location filename="../Function_FileProcess.hpp" line="240"/>
        <source>%1.&quot;%2&quot;：发现完全重叠的Note坐标：</source>
        <translation>%1.&quot;%2&quot;: Coordinate of the note have completely overlapped has been found: </translation>
    </message>
    <message>
        <location filename="../Function_FileProcess.hpp" line="241"/>
        <source>%1.&quot;%2&quot;：发现疑似重叠的Note坐标：</source>
        <translation>%1.&quot;%2&quot;: Coordinate of the note is possible overlap has been found: </translation>
    </message>
    <message>
        <location filename="../Function_FileProcess.hpp" line="247"/>
        <source>%1.&quot;%2&quot;：Note宽度已窄至不可见：</source>
        <translation>%1.&quot;%2&quot;: Size of the note is already less than 0.0: </translation>
    </message>
    <message>
        <location filename="../Function_FileProcess.hpp" line="254"/>
        <source>%1.&quot;%2&quot;：Note时间早于音频起点：</source>
        <translation>%1.&quot;%2&quot;: Time of the note is already earlier than 0:00: </translation>
    </message>
    <message>
        <location filename="../Function_FileProcess.hpp" line="264"/>
        <source>%1.&quot;%2&quot;：Note音效延迟已短于无延迟：</source>
        <translation>%1.&quot;%2&quot;: Sound delay of the note is already less than 0.0: </translation>
    </message>
    <message>
        <location filename="../Function_FileProcess.hpp" line="267"/>
        <source>%1.&quot;%2&quot;：Note音效时长已短于不持续：</source>
        <translation>%1.&quot;%2&quot;: Sound duration of the note is already less than 0.0: </translation>
    </message>
    <message>
        <location filename="../Function_FileProcess.hpp" line="269"/>
        <location filename="../Function_FileProcess.hpp" line="271"/>
        <source>%1.&quot;%2&quot;：Note音效时长同等于不持续：</source>
        <translation>%1.&quot;%2&quot;: Sound duration of the note is equivalent to 0.0: </translation>
    </message>
    <message>
        <location filename="../Function_FileProcess.hpp" line="273"/>
        <source>%1.&quot;%2&quot;：Note音效音高超出有效范围(0~127)：</source>
        <translation>%1.&quot;%2&quot;: Sound pitch of the note is not in the valid range (0~127): </translation>
    </message>
    <message>
        <location filename="../Function_FileProcess.hpp" line="275"/>
        <source>%1.&quot;%2&quot;：Note音效音量超出有效范围(0~127)：</source>
        <translation>%1.&quot;%2&quot;: Sound volume of the note is not in the valid range (0~127): </translation>
    </message>
    <message>
        <location filename="../Function_FileProcess.hpp" line="296"/>
        <location filename="../Function_FileProcess.hpp" line="334"/>
        <location filename="../Function_FileProcess.hpp" line="414"/>
        <location filename="../Function_FileProcess.hpp" line="427"/>
        <location filename="../Function_FileProcess.hpp" line="458"/>
        <location filename="../Function_FileProcess.hpp" line="577"/>
        <location filename="../Function_FileProcess.hpp" line="637"/>
        <location filename="../Function_FileProcess.hpp" line="650"/>
        <location filename="../Function_FileProcess.hpp" line="695"/>
        <location filename="../Function_FileProcess.hpp" line="754"/>
        <source>%1.&quot;%2&quot;：键值缺失：</source>
        <translation>%1.&quot;%2&quot;: Key value is missing: </translation>
    </message>
    <message>
        <location filename="../Function_FileProcess.hpp" line="298"/>
        <location filename="../Function_FileProcess.hpp" line="755"/>
        <source>%1.&quot;%2&quot;：键值缺失(已添加为默认值)：</source>
        <translation>%1.&quot;%2&quot;: Key value is missing (added as default): </translation>
    </message>
    <message>
        <location filename="../Function_FileProcess.hpp" line="305"/>
        <location filename="../Function_FileProcess.hpp" line="375"/>
        <location filename="../Function_FileProcess.hpp" line="392"/>
        <location filename="../Function_FileProcess.hpp" line="406"/>
        <location filename="../Function_FileProcess.hpp" line="419"/>
        <location filename="../Function_FileProcess.hpp" line="432"/>
        <location filename="../Function_FileProcess.hpp" line="450"/>
        <location filename="../Function_FileProcess.hpp" line="463"/>
        <location filename="../Function_FileProcess.hpp" line="479"/>
        <location filename="../Function_FileProcess.hpp" line="532"/>
        <location filename="../Function_FileProcess.hpp" line="569"/>
        <location filename="../Function_FileProcess.hpp" line="615"/>
        <location filename="../Function_FileProcess.hpp" line="629"/>
        <location filename="../Function_FileProcess.hpp" line="642"/>
        <location filename="../Function_FileProcess.hpp" line="655"/>
        <location filename="../Function_FileProcess.hpp" line="669"/>
        <location filename="../Function_FileProcess.hpp" line="687"/>
        <location filename="../Function_FileProcess.hpp" line="700"/>
        <location filename="../Function_FileProcess.hpp" line="744"/>
        <location filename="../Function_FileProcess.hpp" line="762"/>
        <source>%1.&quot;%2&quot;：类型错误：</source>
        <translation>%1.&quot;%2&quot;: Type is incorrect: </translation>
    </message>
    <message>
        <location filename="../Function_FileProcess.hpp" line="307"/>
        <location filename="../Function_FileProcess.hpp" line="763"/>
        <source>%1.&quot;%2&quot;：类型错误(已替换为默认值)：</source>
        <translation>%1.&quot;%2&quot;: Type is incorrect (added as default): </translation>
    </message>
    <message>
        <location filename="../Function_FileProcess.hpp" line="314"/>
        <location filename="../Function_FileProcess.hpp" line="770"/>
        <source>%1.&quot;%2&quot;：数值过小：</source>
        <translation>%1.&quot;%2&quot;: Value too small: </translation>
    </message>
    <message>
        <location filename="../Function_FileProcess.hpp" line="316"/>
        <location filename="../Function_FileProcess.hpp" line="771"/>
        <source>%1.&quot;%2&quot;：数值过小(已替换为默认值)：</source>
        <translation>%1.&quot;%2&quot;: Value too small (added as default): </translation>
    </message>
    <message>
        <location filename="../Function_FileProcess.hpp" line="329"/>
        <location filename="../Function_FileProcess.hpp" line="555"/>
        <source>%1.&quot;%2&quot;：内容为空：</source>
        <translation>%1.&quot;%2&quot;: Content is empty: </translation>
    </message>
    <message>
        <location filename="../Function_FileProcess.hpp" line="351"/>
        <location filename="../Function_FileProcess.hpp" line="594"/>
        <source>%1.&quot;%2&quot;：顺列偏移%3，&quot;$id&quot;为%4时开始：</source>
        <translation>%1.&quot;%2&quot;: Sort order is shifted by %3, start when &quot;$id&quot; is %4: </translation>
    </message>
    <message>
        <location filename="../Function_FileProcess.hpp" line="358"/>
        <location filename="../Function_FileProcess.hpp" line="601"/>
        <source>%1.&quot;%2&quot;：顺列混乱，&quot;$id&quot;为%4时开始：</source>
        <translation>%1.&quot;%2&quot;: Sort order is chaotic, start when &quot;$id&quot; is %4: </translation>
    </message>
    <message>
        <location filename="../Function_FileProcess.hpp" line="363"/>
        <location filename="../Function_FileProcess.hpp" line="606"/>
        <source>%1.&quot;%2&quot;：顺列偏移%3(%4)，&quot;$id&quot;为%5时开始：</source>
        <translation>%1.&quot;%2&quot;: Sort order is shifted by %3(total%4), start when &quot;$id&quot; is %5: </translation>
    </message>
    <message>
        <location filename="../Function_FileProcess.hpp" line="500"/>
        <location filename="../Function_FileProcess.hpp" line="507"/>
        <location filename="../Function_FileProcess.hpp" line="715"/>
        <location filename="../Function_FileProcess.hpp" line="722"/>
        <source>%1.&quot;%2&quot;：内容错误：</source>
        <translation>%1.&quot;%2&quot;: Content error: </translation>
    </message>
    <message>
        <location filename="../Function_FileProcess.hpp" line="522"/>
        <location filename="../Function_FileProcess.hpp" line="737"/>
        <source>%1.&quot;%2&quot;：标记虚指：</source>
        <translation>%1.&quot;%2&quot;: Tag to empty: </translation>
    </message>
</context>
</TS>
