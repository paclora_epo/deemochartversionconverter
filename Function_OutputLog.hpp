﻿#pragma execution_character_set("UTF-8")
#ifndef FUNCTION_OUTPUTLOG_HPP
#define FUNCTION_OUTPUTLOG_HPP

#include <QDebug>
#include <QScrollBar>
#include <QBitmap>
#include <QStringListModel>
#include <QStandardItem>

#include "ui_Class_AppWindow_MainWindow.h"
#include "Class_AppWindow_MainWindow.h"
#include "Class_CustomComponent.h"



// 来自Main.cpp的共享指针.
extern Ui::AppWindow_MainWindow* AppWindow_BaseUi_UiShare;

// 来自Main.cpp的数据模型.
extern QStandardItemModel* 日志列表;

//来自 Class_AppWindow_MainWindow.cpp全局变量.
extern bool 日志列表主动刷新计时器_许可;

enum 信息类型{流程=1, 警告=2, 问题=3, 调试=4};


// 将信息发送至日志面板.
// - 信息类型: 枚举[ 流程 || 警告 || 问题 || 调试 ],其颜色分别为[ 绿色 / 黄色 / 红色 / 蓝色 ].
// - 信息内容: 接收任何形式的QString.
/*Q_INVOKABLE*/ const void 添加日志面板信息(int 信息类型, QString 信息内容) {
	QStandardItem* 临时信息 = new QStandardItem(信息内容);
	switch (信息类型) {
		case 流程: 临时信息->setBackground(QBrush(QColor(143,181,65),QBitmap(":/Image/自右向左渐变网点.png"))); break;
		case 警告: 临时信息->setBackground(QBrush(QColor(222,196,78),QBitmap(":/Image/自右向左渐变网点.png"))); break;
		case 问题: 临时信息->setBackground(QBrush(QColor(221,83,103),QBitmap(":/Image/自右向左渐变网点.png"))); break;
		case 调试: 临时信息->setBackground(QBrush(QColor(71,182,183),QBitmap(":/Image/自右向左渐变网点.png"))); break;
		default: qDebug() << "\"lstwg_OutputPanel\":未知的日志类型:" << 信息类型; delete 临时信息; return; }
	日志列表->appendRow(临时信息);
	//日志面板->setCurrentRow(日志面板->count() - 1); 日志面板->clearSelection(); // 此种形式会取消用户的选择,属于无奈之选. // QListWidget实现.
	//日志面板->scrollToItem(日志面板->item(日志面板->count()-1)); // 此种形式在子线程中无法使用. // QListWidget实现.
	//QMetaObject::invokeMethod(AppWindow_BaseUi_UiShare->lstvw_OutputPanel,[=](){AppWindow_BaseUi_UiShare->lstvw_OutputPanel->scrollTo(日志列表->indexFromItem(日志列表->item(日志列表->rowCount()-1))); }); // 引发线程阻塞.
	//QMetaObject::invokeMethod(AppWindow_BaseUi_UiShare->lstvw_OutputPanel,[=](){AppWindow_BaseUi_UiShare->lstvw_OutputPanel->scrollToBottom(); }); // 引发线程阻塞.
	// 关于invoke https://blog.csdn.net/gongjianbo1992/article/details/105171077/	https://blog.csdn.net/luoyayun361/article/details/97915133
	日志列表主动刷新计时器_许可 = true;
}



#endif  // FUNCTION_OUTPUTLOG_HPP
