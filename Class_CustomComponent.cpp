﻿#include "Class_CustomComponent.h"


PushButton_Custom::PushButton_Custom(QWidget * parent) : QPushButton(parent) {
}
void PushButton_Custom::enterEvent(QEvent *) {
	emit mouseEnter();
}
void PushButton_Custom::leaveEvent(QEvent *) {
	emit mouseLeave();
}

LineEdit_Custom::LineEdit_Custom(QWidget * parent) : QLineEdit(parent) {
}
void LineEdit_Custom::enterEvent(QEvent *) {
	emit mouseEnter();
}
void LineEdit_Custom::leaveEvent(QEvent *) {
	emit mouseLeave();
}

CheckBox_Custom::CheckBox_Custom(QWidget * parent) : QCheckBox(parent) {
}
void CheckBox_Custom::enterEvent(QEvent *) {
	emit mouseEnter();
}
void CheckBox_Custom::leaveEvent(QEvent *) {
	emit mouseLeave();
}

RadioButton_Custom::RadioButton_Custom(QWidget * parent) : QRadioButton(parent) {
}
void RadioButton_Custom::enterEvent(QEvent *) {
	emit mouseEnter();
}
void RadioButton_Custom::leaveEvent(QEvent *) {
	emit mouseLeave();
}

ListView_Custom::ListView_Custom(QWidget * parent) : QListView(parent) {
}
void ListView_Custom::enterEvent(QEvent *) {
	emit mouseEnter();
}
void ListView_Custom::leaveEvent(QEvent *) {
	emit mouseLeave();
}
//void ListView_Custom::rowsInserted(const QModelIndex &parent, int start, int end) {
	// https://doc.qt.io/qt-5/qabstractitemview.html#rowsInserted
	//reset(); scrollToBottom(); // 同步速率过快导致的线程阻塞.
//}
QModelIndexList ListView_Custom::selectedIndexs() {
	return selectedIndexes();
}

TreeView_Custom::TreeView_Custom(QWidget * parent) : QTreeView(parent) {
}
void TreeView_Custom::enterEvent(QEvent *) {
	emit mouseEnter();
}
void TreeView_Custom::leaveEvent(QEvent *) {
	emit mouseLeave();
}
void TreeView_Custom::mouseDoubleClickEvent(QMouseEvent*) {
	emit mouseDblClick();
}
void TreeView_Custom::dragEnterEvent(QDragEnterEvent * event) {
	if (event->mimeData()->hasUrls()) { // 如果MIME包含链接.
		if (event->mimeData()->urls().begin()->isLocalFile()) { // 如果链接列表的首位是本地文件.	https://doc.qt.io/qt-5/qurl.html#isLocalFile
			event->acceptProposedAction();
			event->setDropAction(Qt::MoveAction); // https://blog.csdn.net/weixin_43935474/article/details/104700208
		}
	}
}
void TreeView_Custom::dragMoveEvent(QDragMoveEvent *) {
	// 此函数可留空但必须做出实现.如有问题请参见成员函数dragEnterEvent.
}
void TreeView_Custom::dropEvent(QDropEvent *event) {
	面板引入文件列表中介.clear();
	foreach (QUrl 检测, event->mimeData()->urls()) {
		if (检测.isLocalFile()) { // 如果链接是本地文件,加入文件列表.
			面板引入文件列表中介.append(检测.toLocalFile()); // https://doc.qt.io/qt-5/qurl.html#toLocalFile
		}
	}
	emit dropFile();
}
QModelIndexList TreeView_Custom::selectedIndexs() {
	return selectedIndexes(); // https://doc.qt.io/qt-5/qtreeview.html#selectedIndexes
}
