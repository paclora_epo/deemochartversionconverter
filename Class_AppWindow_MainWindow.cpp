﻿#include "ui_Class_AppWindow_MainWindow.h"
#include "Class_AppWindow_MainWindow.h"
#include "Class_CustomComponent.h"
#include "Function_FileProcess.hpp"
#include "Function_OutputLog.hpp"
#include "ExternLibRef/json.hpp"

#include <QTranslator>
#include <QDebug>
#include <QMenu>
#include <QClipboard>
#include <QStandardItemModel>
#include <QtConcurrent>


// 来自Main.cpp的数据模型
extern QStandardItemModel* 文件列表;
extern QStandardItemModel* 日志列表;

// 创建用于控制计时器循环的判定值.
bool 日志列表主动刷新计时器_许可{false};

AppWindow_MainWindow::AppWindow_MainWindow(QWidget * parent) : QMainWindow(parent) , UiSelf(new Ui::AppWindow_MainWindow) {
	UiSelf->setupUi(this);

	// QSS
	QFile QSS(":/QSS/静态样式.qss"); QSS.open(QFile::ReadOnly);
	this->setStyleSheet(QString(QTextStream(&QSS).readAll())); QSS.close();
	UiSelf->progressBar->setStyleSheet(QString( // 进度条样式.
		"QProgressBar, QProgressBar::chunk { Border-Style: solid; Border-Color: transparent; Border-Width: 0px; Border-Radius: 2px; }"
		"QProgressBar { Background-Color: #EAEAEA; }"
		"QProgressBar::chunk { Background-Color: %1; }" ).arg(系统主题颜色.isValid() ? 系统主题颜色.name(QColor::HexRgb) : "#47B6B7"));
	QString 单选框样式(QString( // 单选框样式.
		"QRadioButton { Background-Color: #FFFFFF; Border-Style: solid; Border-Color: #EAEAEA; Border-Width: 2px; Border-Radius: 4px; }"
		"QRadioButton:hover { Border-Color: #E3E3E3; Border-Width: 3px; }"
		"QRadioButton:pressed { Background-Color: #F7F7F7; }"
		"QRadioButton:checked { Border-Color: %1; Border-Width: 3px; }"
		"QRadioButton:checked:disabled { Border-Color: #EAEAEA; }"
		"QRadioButton::indicator { Width: 0px; Height: 0px; }" ).arg(系统主题颜色.isValid() ? 系统主题颜色.name(QColor::HexRgb) : "#47B6B7"));
	UiSelf->rdb_V1->setStyleSheet(单选框样式); UiSelf->rdb_V2->setStyleSheet(单选框样式); UiSelf->rdb_V3->setStyleSheet(单选框样式); UiSelf->rdb_CheckOnly->setStyleSheet(单选框样式);
	QString 复选框样式(QString( // 复选框样式.
		"QCheckBox { Background-Color: QLinearGradient(x1:0, y1:0.5, x2:1, y2:0.5 stop:0.2 #FFFFFF, stop:0.8 transparent);"
			"Border-Style: solid; Border-Color: QLinearGradient(x1:0, y1:0.5, x2:1, y2:0.5 stop:0.2 #EAEAEA, stop:0.8 transparent);"
			"Border-Left-Color: #EAEAEA; Border-Right-Color: transparent;"
			"Border-Width: 2px; Border-Radius: 4px; Border-Top-Right-Radius: 0px; Border-Bottom-Right-Radius: 0px; }"
		"QCheckBox:hover { Border-Color: QLinearGradient(x1:0, y1:0.5, x2:1, y2:0.5 stop:0.2 #E3E3E3, stop:0.8 transparent);"
			"Border-Left-Color: #E3E3E3; Border-Width: 3px; Border-Right-Width: 0px; }"
		"QCheckBox:pressed { Background-Color: #F7F7F7; }"
		"QCheckBox:checked { Border-Color: QLinearGradient(x1:0, y1:0.5, x2:1, y2:0.5 stop:0.2 %1, stop:0.75 transparent);"
			"Border-Left-Color: %1; Border-Width: 3px; Border-Left-Width: 3px; Border-Right-Width: 0px; }"
		"QCheckBox:checked:disabled { Border-Color: QLinearGradient(x1:0, y1:0.5, x2:1, y2:0.5 stop:0.2 #EAEAEA, stop:0.8 transparent);"
			"Border-Left-Color: #EAEAEA; Border-Right-Color: transparent; }"
		"QCheckBox::indicator { Width: 0px; Height: 0px; }" ).arg(系统主题颜色.isValid() ? 系统主题颜色.name(QColor::HexRgb) : "#47B6B7"));
	UiSelf->ckb_BoundsOut->setStyleSheet(复选框样式); UiSelf->ckb_Overlapping->setStyleSheet(复选框样式); UiSelf->ckb_TooSmall->setStyleSheet(复选框样式); UiSelf->ckb_TooEarly->setStyleSheet(复选框样式);
	UiSelf->ckb_AtypicalValue->setStyleSheet(复选框样式); UiSelf->ckb_SkipInvalidSoundsDelay->setStyleSheet(复选框样式); UiSelf->ckb_KeepPath->setStyleSheet(复选框样式);
	QSS_菜单样式.append( // 菜单样式.在创建菜单时设置,字体颜色需独立设置.
		"QMenu { Background-Color: #FFFFFF; Color: #444444; Border-Style: solid; Border-Color: #EAEAEA; Border-Width: 2px; Border-Radius: 4px; }"
		"QMenu::item { Background-Color: transparent; Margin-Top: 2px; Margin-Bottom: 2px; Margin-Left: 2px; Margin-Right: 2px; }"
		"QMenu::item:text { Padding-Top: 3px; Padding-Bottom: 3px; Padding-Left: 8px; Padding-Right: 5px; }"
		"QMenu::item:selected { Background-Color: #EAEAEA; Border-Radius: 4px; }"
		"QMenu::separator { Background: #EAEAEA; Margin-Left: 10px; Margin-Right: 7px; Height: 2px; }" );

	// 绑定组件的信息提示
	提示面板内容绑定();

	// 其它功能性绑定
	connect(UiSelf->tvw_FilePanel, &TreeView_Custom::mouseDblClick, this, [=](){QtConcurrent::run(this,&AppWindow_MainWindow::面板引入文件,弹窗); }); // https://doc.qt.io/qt-5/qtconcurrentrun.html
	connect(UiSelf->tvw_FilePanel, &TreeView_Custom::dropFile, this, [=](){QtConcurrent::run(this,&AppWindow_MainWindow::面板引入文件,拖拽); });
	connect(日志列表主动刷新计时器, &QTimer::timeout, this, [=](){if (日志列表主动刷新计时器_许可) {UiSelf->lstvw_OutputPanel->scrollToBottom();} 日志列表主动刷新计时器_许可 = false; });

	// 初始化文件列表面板
	UiSelf->tvw_FilePanel->setModel(文件列表);
	文件列表->setHorizontalHeaderLabels(QStringList() << QString("#") << QString(tr("文件名")) << QString(tr("版本")) << QString(tr("状态")) << QString(tr("路径")));
	文件列表->horizontalHeaderItem(0)->setTextAlignment(Qt::AlignRight); //https://doc.qt.io/qt-5/qstandarditemmodel.html#horizontalHeaderItem
	文件列表->horizontalHeaderItem(2)->setTextAlignment(Qt::AlignCenter); // https://doc.qt.io/qt-5/qt.html#AlignmentFlag-enum
	文件列表->horizontalHeaderItem(3)->setTextAlignment(Qt::AlignCenter);
	UiSelf->tvw_FilePanel->setColumnWidth(0,27); // http://www.cnblogs.com/dachen408/p/7206738.html
	UiSelf->tvw_FilePanel->setColumnWidth(1,130); // https://blog.csdn.net/weixin_43850620/article/details/107811644
	UiSelf->tvw_FilePanel->setColumnWidth(2,45);
	UiSelf->tvw_FilePanel->setColumnWidth(3,49);
	UiSelf->tvw_FilePanel->setColumnWidth(4,27);

	// 初始化日志列表面板
	UiSelf->lstvw_OutputPanel->setModel(日志列表);

	// 启动计时器
	日志列表主动刷新计时器->start(125);
}

AppWindow_MainWindow::~AppWindow_MainWindow() {
	delete UiSelf;
}

void AppWindow_MainWindow::on_lstvw_OutputPanel_customContextMenuRequested(const QPoint &pos) {
	// 参考已弃用	https://blog.csdn.net/Q1302182594/article/details/46120321
	QModelIndex 控制点 = UiSelf->lstvw_OutputPanel->indexAt(pos);
	QMenu 条目菜单;
	if (!控制点.isValid()) { // 如果落点无效,省略部分选项.
		条目菜单.addAction(tr("复制全部条目"), this, [=](){on_lstvw_OutputPanel_customContextMenu_Action_CopyAllItem(); });
		条目菜单.addSeparator();
		条目菜单.addAction(tr("清除全部条目"), this, [=](){on_lstvw_OutputPanel_customContextMenu_Action_ClearAllItem(); }); }
	else { // 如果落点有item,完全创建菜单.
		条目菜单.addAction(tr("复制选中条目"), this, [=](){on_lstvw_OutputPanel_customContextMenu_Action_CopySelectedItem(); });
		条目菜单.addAction(tr("复制全部条目"), this, [=](){on_lstvw_OutputPanel_customContextMenu_Action_CopyAllItem(); });
		条目菜单.addSeparator();
		条目菜单.addAction(tr("清除选中条目"), this, [=](){on_lstvw_OutputPanel_customContextMenu_Action_ClearSelectedItem(); });
		条目菜单.addAction(tr("清除全部条目"), this, [=](){on_lstvw_OutputPanel_customContextMenu_Action_ClearAllItem(); }); }
	条目菜单.setAttribute(Qt::WA_TranslucentBackground); 条目菜单.setWindowFlags(条目菜单.windowFlags() | Qt::FramelessWindowHint | Qt::NoDropShadowWindowHint); 条目菜单.setStyleSheet(QSS_菜单样式); // https://blog.csdn.net/qq_41673920/article/details/116980362
	条目菜单.exec(QCursor::pos());
}
void AppWindow_MainWindow::on_lstvw_OutputPanel_customContextMenu_Action_CopySelectedItem() {
	QModelIndexList 已选 = UiSelf->lstvw_OutputPanel->selectedIndexs();
	QString 剪切内容;
	while (!(已选.isEmpty())) {
		剪切内容.append(已选.takeFirst().data().value<QString>());
		剪切内容.append("\n"); }
	QClipboard *剪切板 = QApplication::clipboard();
	剪切板->setText(剪切内容);
}
void AppWindow_MainWindow::on_lstvw_OutputPanel_customContextMenu_Action_CopyAllItem() {
	QString 剪切内容; // https://bbs.csdn.net/topics/390978610
	for (int i = 0; i < 日志列表->rowCount(); i++) {
		剪切内容.append(日志列表->index(i,0).data().value<QString>());
		剪切内容.append("\n"); }
	QClipboard *剪切板 = QApplication::clipboard();
	剪切板->setText(剪切内容);
}
void AppWindow_MainWindow::on_lstvw_OutputPanel_customContextMenu_Action_ClearSelectedItem() {
	QModelIndexList 已选; unsigned int 总选 = UiSelf->lstvw_OutputPanel->selectedIndexs().count();
	for (unsigned int i = 0; i < 总选; i++) { // 实现摘抄自文件面板的菜单.谁也想不到这程序会出现第二个数据模型(ListWidget卡顿严重).
		已选 = UiSelf->lstvw_OutputPanel->selectedIndexs();
		QList<QStandardItem*> 行合集 = 日志列表->takeRow(已选.takeFirst().row()); // https://doc.qt.io/qt-5/qlist.html#takeFirst
		foreach (QStandardItem* i, 行合集) delete i; }
	AppWindow_BaseUi_UiShare->lstvw_OutputPanel->scrollTo(UiSelf->lstvw_OutputPanel->currentIndex());
}
void AppWindow_MainWindow::on_lstvw_OutputPanel_customContextMenu_Action_ClearAllItem() {
	for (unsigned int i = 日志列表->rowCount()-1; (i+1) >0; i--) {
		QList<QStandardItem*> 行合集 = 日志列表->takeRow(i);
		foreach (QStandardItem* i, 行合集) delete i; }
}

void AppWindow_MainWindow::on_tvw_FilePanel_customContextMenuRequested(const QPoint &pos) {
	// https://www.freesion.com/article/1617301717/
	QModelIndex 控制点 = UiSelf->tvw_FilePanel->indexAt(pos);
	QMenu 条目菜单;
	条目菜单.addAction(tr("引入文件..."), this, [=](){on_tvw_FilePanel_customContextMenu_Action_ImportFile(); });
	if (!控制点.isValid()) { // 如果落点无效,不创建"移除选中条目".
		if (文件列表->rowCount() > 0) { //如果列表为空,不创建"移除所有条目".
			条目菜单.addSeparator();
			条目菜单.addAction(tr("移除所有条目"), this, [=](){on_tvw_FilePanel_customContextMenu_Action_ClearAllItem(); }); }}
	else { // 如果落点有效,完全创建菜单.
		条目菜单.addSeparator();
		条目菜单.addAction(tr("移除所选条目"), this, [=](){on_tvw_FilePanel_customContextMenu_Action_ClearSelectedItem(); });
		条目菜单.addAction(tr("移除所有条目"), this, [=](){on_tvw_FilePanel_customContextMenu_Action_ClearAllItem(); }); }
	条目菜单.setAttribute(Qt::WA_TranslucentBackground); 条目菜单.setWindowFlags(条目菜单.windowFlags() | Qt::FramelessWindowHint | Qt::NoDropShadowWindowHint); 条目菜单.setStyleSheet(QSS_菜单样式);
	条目菜单.exec(QCursor::pos());
}
void AppWindow_MainWindow::on_tvw_FilePanel_customContextMenu_Action_ImportFile() {
	QtConcurrent::run(this,&AppWindow_MainWindow::面板引入文件,弹窗);
}
void AppWindow_MainWindow::on_tvw_FilePanel_customContextMenu_Action_ClearSelectedItem() {
	移除选中条目(UiSelf->tvw_FilePanel,文件列表);
	刷新条目编号(文件列表);
	if (文件列表->rowCount() == 0) {UiSelf->progressBar->reset(); } // 如果列表为空则重置进度条.
 }
void AppWindow_MainWindow::on_tvw_FilePanel_customContextMenu_Action_ClearAllItem() {
	for (unsigned int i = 文件列表->rowCount(); i > 0; i--)
		移除单行条目(文件列表,i-1);
	if (文件列表->rowCount() == 0) {UiSelf->progressBar->reset(); } // 如果列表为空则重置进度条.
}

void AppWindow_MainWindow::on_ckb_BoundsOut_stateChanged(int arg1) {
	switch (arg1) {case Qt::Checked: 转换参数::超界警告 = true; break;  case Qt::Unchecked: 转换参数::超界警告 = false; break; }
}
void AppWindow_MainWindow::on_ckb_Overlapping_stateChanged(int arg1) {
	switch (arg1) {case Qt::Checked: 转换参数::重叠警告 = true; break;  case Qt::Unchecked: 转换参数::重叠警告 = false; break; }
}
void AppWindow_MainWindow::on_ckb_TooSmall_stateChanged(int arg1) {
	switch (arg1) {case Qt::Checked: 转换参数::过小警告 = true; break;  case Qt::Unchecked: 转换参数::过小警告 = false; break; }
}
void AppWindow_MainWindow::on_ckb_TooEarly_stateChanged(int arg1) {
	switch (arg1) {case Qt::Checked: 转换参数::过早警告 = true; break;  case Qt::Unchecked: 转换参数::过早警告 = false; break; }
}
void AppWindow_MainWindow::on_ckb_AtypicalValue_stateChanged(int arg1) {
	switch (arg1) {case Qt::Checked: 转换参数::离谱警告 = true; break;  case Qt::Unchecked: 转换参数::离谱警告 = false; break; }
}
void AppWindow_MainWindow::on_ckb_SkipInvalidSoundsDelay_stateChanged(int arg1) {
	switch (arg1) {case Qt::Checked: 转换参数::跳过空值 = true; break;  case Qt::Unchecked: 转换参数::跳过空值 = false; break; }
}
void AppWindow_MainWindow::on_ckb_KeepPath_stateChanged(int arg1) {
	switch (arg1) {case Qt::Checked: 转换参数::保持路径 = true; break;  case Qt::Unchecked: 转换参数::保持路径 = false; break; }
}
void AppWindow_MainWindow::on_rdb_CheckOnly_toggled(bool checked) {
	if (checked) {转换参数::目标版本 = 0; UiSelf->ckb_SkipInvalidSoundsDelay->setEnabled(false); UiSelf->ckb_SkipInvalidSoundsDelay->setChecked(false); UiSelf->ckb_KeepPath->setEnabled(false); }
	else {UiSelf->ckb_SkipInvalidSoundsDelay->setEnabled(true); UiSelf->ckb_KeepPath->setEnabled(true); }
	if (后缀编辑框可自动修改) {UiSelf->lineEdit_Suffix->setText(""); }
}
void AppWindow_MainWindow::on_rdb_V1_toggled(bool checked) {
	if (checked) {转换参数::目标版本 = 1; UiSelf->ckb_SkipInvalidSoundsDelay->setChecked(true); }
	else {UiSelf->ckb_SkipInvalidSoundsDelay->setChecked(false); }
	if (后缀编辑框可自动修改) {UiSelf->lineEdit_Suffix->setText("_v1.json"); }
}
void AppWindow_MainWindow::on_rdb_V2_toggled(bool checked) {
	if (checked) {转换参数::目标版本 = 3; }
	if (后缀编辑框可自动修改) {UiSelf->lineEdit_Suffix->setText("_v3.json"); }
}
void AppWindow_MainWindow::on_rdb_V3_toggled(bool checked) {
	if (checked) {转换参数::目标版本 = 4; }
	if (后缀编辑框可自动修改) {UiSelf->lineEdit_Suffix->setText("_v4.json"); }
}

void AppWindow_MainWindow::on_lineEdit_Suffix_textEdited(const QString &arg1) {
	后缀编辑框可自动修改 = false;
}

void AppWindow_MainWindow::on_btn_LanguageSwitch_clicked() {
	extern QTranslator* 本地化; // 来自"Main.cpp"的全局变量
	do { // 依据返回值重新装载语言文件
	if (本地化->language() == "zh_CN") {
		qApp->removeTranslator(本地化); delete 本地化; 本地化 = new QTranslator; // 原void函数,释放指针和重指.
		bool 本地化读取状态 = 本地化->load(":/language/DeemoChartConverter_Qt_zh_TW.qm");
		qDebug() << "\"btn_LanguageSwitch_clicked()\"的本化文件的读取状态为" << 本地化读取状态 << "->" << 本地化->language();
		//添加日志面板信息(调试,QString("\"btn_LanguageSwitch_clicked()\"的本化文件的读取状态为%1->%2").arg(QVariant(本地化读取状态).toString(), 本地化->language()));
		break; }
	if (本地化->language() == "zh_TW") {
		qApp->removeTranslator(本地化); delete 本地化; 本地化 = new QTranslator; // 原void函数,释放指针和重指.
		bool 本地化读取状态 = 本地化->load(":/language/DeemoChartConverter_Qt_en_US.qm");
		qDebug() << "\"btn_LanguageSwitch_clicked()\"的本化文件的读取状态为" << 本地化读取状态 << "->" << 本地化->language();
		//添加日志面板信息(调试,QString("\"btn_LanguageSwitch_clicked()\"的本化文件的读取状态为%1->%2").arg(QVariant(本地化读取状态).toString(), 本地化->language()));
		break; }
	if (本地化->language() == "en_US") {
		qApp->removeTranslator(本地化); delete 本地化; 本地化 = new QTranslator; // 原void函数,释放指针和重指.
		bool 本地化读取状态 = 本地化->load(":/language/DeemoChartConverter_Qt_zh_CN.qm");
		qDebug() << "\"btn_LanguageSwitch_clicked()\"的本化文件的读取状态为" << 本地化读取状态 << "->" << 本地化->language();
		//添加日志面板信息(调试,QString("\"btn_LanguageSwitch_clicked()\"的本化文件的读取状态为%1->%2").arg(QVariant(本地化读取状态).toString(), 本地化->language()));
		break; }
	} while (false);
	qApp->installTranslator(本地化);
	UiSelf->retranslateUi(this); // 刷新UI
	// 以下为特殊刷新.
	提示面板内容绑定(); // 从构造函数中分离,为配合数据再分配.
	文件列表->setHorizontalHeaderLabels(QStringList() << QString("#") << QString(tr("文件名")) << QString(tr("版本")) << QString(tr("状态")) << QString(tr("路径"))); // 来自构造函数初始化部分,为不再分离为独立函数.
}

void AppWindow_MainWindow::on_btn_CheckFileVersion_clicked() {
	for (int i = 0; i < 文件列表->rowCount(); i++) { // 对于此种高速检查暂不考虑效率问题,即便一次性检查上千条也不会有太长等待.
		刷新条目版本(文件列表, i, 获取单个文件版本(文件列表,i)); }
}

void AppWindow_MainWindow::on_btn_ConvertAllFile_clicked() {
	QtConcurrent::run(this,&AppWindow_MainWindow::批量转换文件);
	//批量转换文件();
}

const void AppWindow_MainWindow::设置提示面板信息(Ui::AppWindow_MainWindow * UiPointer, const QString &信息详情) {
	UiPointer->lable_TipsPanel->setText(信息详情);
}

void AppWindow_MainWindow::面板引入文件(int 文件引入方式) {
	QMetaObject::invokeMethod(this,[=]() { // 异步下操控主线程UI的方法.
		UiSelf->groupBox_OptionArea->setEnabled(false);
		UiSelf->tvw_FilePanel->setEnabled(false); });

	unsigned int 前计数 = 文件列表->rowCount();
	QList<QString> 预引入文件;

	switch(文件引入方式) {
		case 弹窗:
			QMetaObject::invokeMethod(this,[=](){弹窗选择文件(UiSelf->CentralWidget_Main, UiSelf->tvw_FilePanel); },Qt::BlockingQueuedConnection); // 阻塞式调用.
			预引入文件 = 引入多个文件(UiSelf->tvw_FilePanel->面板引入文件列表中介);
			UiSelf->tvw_FilePanel->面板引入文件列表中介.clear();
			break;
		case 拖拽:
			预引入文件 = 引入多个文件(UiSelf->tvw_FilePanel->面板引入文件列表中介);
			UiSelf->tvw_FilePanel->面板引入文件列表中介.clear(); // 必须清空的成员变量.
			break; }

	for (QList<QString>::iterator i = 预引入文件.begin(); i != 预引入文件.end(); i++) {
		nlohmann::json* 有效性 = 检查单个文件(*i);
		if(有效性 != nullptr)
			创建单行数据到模型(UiSelf->tvw_FilePanel,文件列表,*i,有效性); }

	unsigned int 引入差 = 文件列表->rowCount()-前计数;
	if (引入差>0)
		添加日志面板信息(流程,QString(QFileDialog::tr("共引入%1个文件。").arg(引入差)));

	QMetaObject::invokeMethod(this,[=]() {
		UiSelf->groupBox_OptionArea->setEnabled(true);
		UiSelf->tvw_FilePanel->setEnabled(true); });
}

void AppWindow_MainWindow::批量转换文件() {
	if (文件列表->rowCount() <= 0) return; // 如果列表为空不执行.

	QMetaObject::invokeMethod(this,[=]() {UiSelf->groupBox_OptionArea->setEnabled(false); UiSelf->tvw_FilePanel->setEnabled(false); });

	unsigned int 错误计数{0}, 警告计数{0}, 错误计数前对比{0}, 警告计数前对比{0}; int 文件数目{文件列表->rowCount()}; bool 数据有效性{false}; QString 当前文件名;
	QVariant* 数据有效性检查 = nullptr; nlohmann::ordered_json* 固位数据结果 = nullptr; /*nlohmann::json* 变位数据结果 = nullptr;*/ std::ofstream 最终输出;

	QMetaObject::invokeMethod(UiSelf->progressBar,[=](){UiSelf->progressBar->setMaximum(文件数目); }); // 设置总长.

	for (int i = 0; i < 文件数目; i++) {
		错误计数前对比 = 错误计数; 警告计数前对比 = 警告计数; 当前文件名 = 文件列表->item(i,1)->text();

		switch (转换参数::目标版本) { // 运行转换,取出指针并确认内容.
			case 0:
				数据有效性检查 = 转换进程_定排1(*文件列表->item(i,5)->data().value<nlohmann::json*>(), i+1, 当前文件名, 警告计数, 错误计数);
				if (数据有效性检查 != nullptr) {数据有效性 = true; }
				break;
			case 1: case 3:
				数据有效性检查 = 转换进程_定排1(*文件列表->item(i,5)->data().value<nlohmann::json*>(), i+1, 当前文件名, 警告计数, 错误计数);
				if (数据有效性检查 != nullptr) {固位数据结果 = 数据有效性检查->value<nlohmann::ordered_json*>(); 数据有效性 = true; } else {数据有效性 = false; }
				break;
			case 4:
				数据有效性检查 = 转换进程_定排2(*文件列表->item(i,5)->data().value<nlohmann::json*>(), i+1, 当前文件名, 警告计数, 错误计数);
				if (数据有效性检查 != nullptr) {固位数据结果 = 数据有效性检查->value<nlohmann::ordered_json*>(); 数据有效性 = true; } else {数据有效性 = false; }
				break; }

		if (数据有效性) { // 如果数据有效,进行附加检查和输出.
			if (转换参数::超界警告) {检查进程_超界(*文件列表->item(i,5)->data().value<nlohmann::json*>(), i+1, 文件列表->item(i,1)->text(), 警告计数); }
			if (转换参数::重叠警告) {检查进程_重叠(*文件列表->item(i,5)->data().value<nlohmann::json*>(), i+1, 文件列表->item(i,1)->text(), 警告计数); }
			if (转换参数::过小警告) {检查进程_过小(*文件列表->item(i,5)->data().value<nlohmann::json*>(), i+1, 文件列表->item(i,1)->text(), 警告计数); }
			if (转换参数::过早警告) {检查进程_过早(*文件列表->item(i,5)->data().value<nlohmann::json*>(), i+1, 文件列表->item(i,1)->text(), 警告计数); }
			if (转换参数::离谱警告) {检查进程_非常(*文件列表->item(i,5)->data().value<nlohmann::json*>(), i+1, 文件列表->item(i,1)->text(), 警告计数); }

			if (转换参数::目标版本 != 0) { // 最终输出.
				if (转换参数::保持路径) { // 如果要求原路径,生成在文件原路径.
					最终输出.open(文件列表->item(i,4)->text().toLocal8Bit().toStdString() + "/" + UiSelf->lineEdit_Prefix->text().toLocal8Bit().toStdString() + 当前文件名.toLocal8Bit().toStdString().substr(0,当前文件名.toLocal8Bit().toStdString().find_last_of('.')) + UiSelf->lineEdit_Suffix->text().toLocal8Bit().toStdString(), std::ios_base::out | std::ios_base::trunc); }
				else { // 如果不要求,则生成在程序路径.
					最终输出.open(UiSelf->lineEdit_Prefix->text().toLocal8Bit().toStdString() + 当前文件名.toLocal8Bit().toStdString().substr(0,当前文件名.toLocal8Bit().toStdString().find_last_of('.')) + UiSelf->lineEdit_Suffix->text().toLocal8Bit().toStdString(), std::ios_base::out | std::ios_base::trunc); }
				if (!最终输出.is_open()) { // 检查文件是否可写入.
					添加日志面板信息(问题,QString(QObject::tr("%1.\"%2\"：写入文件时出错。")).arg(i+1).arg(当前文件名)); ++错误计数; }
				最终输出 << *固位数据结果;
				最终输出.clear(); 最终输出.close(); }

		delete 固位数据结果; /*delete 变位数据结果;*/ delete 数据有效性检查;
		固位数据结果 = nullptr; /*变位数据结果 = nullptr;*/ 数据有效性检查 = nullptr;

		if (错误计数前对比 < 错误计数) { // 根据计数确定最终转换状态.
			刷新转换状态(UiSelf->tvw_FilePanel,文件列表,i,错误); }
		else { // 如果没有错误.
			if (警告计数前对比 < 警告计数) { // 如果有警告,则瑕疵.
				刷新转换状态(UiSelf->tvw_FilePanel,文件列表,i,瑕疵); }
			else{ // 如果全无,说明无误.
				刷新转换状态(UiSelf->tvw_FilePanel,文件列表,i,完成); }}}

		QMetaObject::invokeMethod(UiSelf->progressBar,[=](){UiSelf->progressBar->setValue(i + 1); }); /* 更新进度条.*/ }

	添加日志面板信息(流程,QString(QFileDialog::tr("%1个警告，%2个错误。")).arg(警告计数).arg(错误计数));

	QMetaObject::invokeMethod(this,[=]() {UiSelf->groupBox_OptionArea->setEnabled(true); UiSelf->tvw_FilePanel->setEnabled(true); });
}

void AppWindow_MainWindow::提示面板内容绑定() {
	QString 空字符串("");
	QString 语言切换按钮(tr("语言切换——可在\"简体中文\"、\"正體中文\"和\"English\"中切换。当前语言为\"简体中文\"。"));
		connect(UiSelf->btn_LanguageSwitch, &PushButton_Custom::mouseEnter, this, [=](){设置提示面板信息(UiSelf, 语言切换按钮); });
		connect(UiSelf->btn_LanguageSwitch, &PushButton_Custom::mouseLeave, this, [=](){设置提示面板信息(UiSelf, 空字符串); });
	QString 检查版本按钮(tr("检查谱面版本——检查谱面的格式版本，结果将显示于文件名右侧。"));
		connect(UiSelf->btn_CheckFileVersion, &PushButton_Custom::mouseEnter, this, [=](){设置提示面板信息(UiSelf, 检查版本按钮); });
		connect(UiSelf->btn_CheckFileVersion, &PushButton_Custom::mouseLeave, this, [=](){设置提示面板信息(UiSelf, 空字符串); });
	QString 开始转换按钮(tr("转换所有谱面——立刻按照所选参数转换所有谱面。"));
		connect(UiSelf->btn_ConvertAllFile, &PushButton_Custom::mouseEnter, this, [=](){设置提示面板信息(UiSelf, 开始转换按钮); });
		connect(UiSelf->btn_ConvertAllFile, &PushButton_Custom::mouseLeave, this, [=](){设置提示面板信息(UiSelf, 空字符串); });
	QString 前缀修正框(tr("文件名前缀——经过转换的文件是以\"前缀原文件名后缀\"的形式的命名的，此处填写的内容将作为\"前缀\"。"));
		connect(UiSelf->lineEdit_Prefix, &LineEdit_Custom::mouseEnter, this, [=](){设置提示面板信息(UiSelf, 前缀修正框); });
		connect(UiSelf->lineEdit_Prefix, &LineEdit_Custom::mouseLeave, this, [=](){设置提示面板信息(UiSelf, 空字符串); });
	QString 后缀修正框(tr("文件名后缀——经过转换的文件是以\"前缀原文件名后缀\"的形式的命名的，此处填写的内容将作为\"后缀\"。"));
		connect(UiSelf->lineEdit_Suffix, &LineEdit_Custom::mouseEnter, this, [=](){设置提示面板信息(UiSelf, 后缀修正框); });
		connect(UiSelf->lineEdit_Suffix, &LineEdit_Custom::mouseLeave, this, [=](){设置提示面板信息(UiSelf, 空字符串); });
	QString 超界复选框(tr("超出轨道边界——勾选此选项可令程序在检查到超出轨道范围的按键时发出警告(包括不可视按键)。"));
		connect(UiSelf->ckb_BoundsOut, &CheckBox_Custom::mouseEnter, this, [=](){设置提示面板信息(UiSelf, 超界复选框); });
		connect(UiSelf->ckb_BoundsOut, &CheckBox_Custom::mouseLeave, this, [=](){设置提示面板信息(UiSelf, 空字符串); });
	QString 重叠复选框(tr("坐标完全重叠——勾选此选项可令程序在检查到时间与位置几乎相同的可视按键时发出警告。"));
		connect(UiSelf->ckb_Overlapping, &CheckBox_Custom::mouseEnter, this, [=](){设置提示面板信息(UiSelf, 重叠复选框); });
		connect(UiSelf->ckb_Overlapping, &CheckBox_Custom::mouseLeave, this, [=](){设置提示面板信息(UiSelf, 空字符串); });
	QString 过小复选框(tr("宽度过小——勾选此选项可令程序在检查到宽度不大于0的按键时发出警告。"));
		connect(UiSelf->ckb_TooSmall, &CheckBox_Custom::mouseEnter, this, [=](){设置提示面板信息(UiSelf, 过小复选框); });
		connect(UiSelf->ckb_TooSmall, &CheckBox_Custom::mouseLeave, this, [=](){设置提示面板信息(UiSelf, 空字符串); });
	QString 过早复选框(tr("时间过早——勾选此选项可令程序在检查到时间小于0的按键时发出警告。"));
		connect(UiSelf->ckb_TooEarly, &CheckBox_Custom::mouseEnter, this, [=](){设置提示面板信息(UiSelf, 过早复选框); });
		connect(UiSelf->ckb_TooEarly, &CheckBox_Custom::mouseLeave, this, [=](){设置提示面板信息(UiSelf, 空字符串); });
	QString 非典型值复选框(tr("音效非常——勾选此选项可令程序在检查到与预期数值范围不符的效果音属性时发出警告。"));
		connect(UiSelf->ckb_AtypicalValue, &CheckBox_Custom::mouseEnter, this, [=](){设置提示面板信息(UiSelf, 非典型值复选框); });
		connect(UiSelf->ckb_AtypicalValue, &CheckBox_Custom::mouseLeave, this, [=](){设置提示面板信息(UiSelf, 空字符串); });
	QString 跳过空值复选框(tr("跳过空值——这是一个转换选项，它可令程序在转换时忽略部分空值。对\"1.0\"版本以外的预期转换使用此选项将导致文件不合规标。"));
		connect(UiSelf->ckb_SkipInvalidSoundsDelay, &CheckBox_Custom::mouseEnter, this, [=](){设置提示面板信息(UiSelf, 跳过空值复选框); });
		connect(UiSelf->ckb_SkipInvalidSoundsDelay, &CheckBox_Custom::mouseLeave, this, [=](){设置提示面板信息(UiSelf, 空字符串); });
	QString 保持路径复选框(tr("保持路径——这是一个转换选项，它可令新文件创建在与源文件相同的文件夹中。"));
		connect(UiSelf->ckb_KeepPath, &CheckBox_Custom::mouseEnter, this, [=](){设置提示面板信息(UiSelf, 保持路径复选框); });
		connect(UiSelf->ckb_KeepPath, &CheckBox_Custom::mouseLeave, this, [=](){设置提示面板信息(UiSelf, 空字符串); });
	QString 仅限检查单选框(tr("仅检查——选择此项可令程序仅按照所选设置检查文件内容，而不会转换出新文件。"));
		connect(UiSelf->rdb_CheckOnly, &RadioButton_Custom::mouseEnter, this, [=](){设置提示面板信息(UiSelf, 仅限检查单选框); });
		connect(UiSelf->rdb_CheckOnly, &RadioButton_Custom::mouseLeave, this, [=](){设置提示面板信息(UiSelf, 空字符串); });
	QString V10版本单选框(tr("转为第1.0版本——最早的谱面格式，也是可视化关卡编辑器平台中最通用的版本。"));
		connect(UiSelf->rdb_V1, &RadioButton_Custom::mouseEnter, this, [=](){设置提示面板信息(UiSelf, V10版本单选框); });
		connect(UiSelf->rdb_V1, &RadioButton_Custom::mouseLeave, this, [=](){设置提示面板信息(UiSelf, 空字符串); });
	QString V30版本单选框(tr("转为第3.0版本——于2.4版本出现的谱面格式，其内容包含部分无效信息，是现存格式中最臃肿的版本。"));
		connect(UiSelf->rdb_V2, &RadioButton_Custom::mouseEnter, this, [=](){设置提示面板信息(UiSelf, V30版本单选框); });
		connect(UiSelf->rdb_V2, &RadioButton_Custom::mouseLeave, this, [=](){设置提示面板信息(UiSelf, 空字符串); });
	QString V40版本单选框(tr("转为第4.0版本——于4.0版本出现的谱面格式，相对于旧版主要统一了数据类型，因此不再与先前版本兼容。"));
		connect(UiSelf->rdb_V3, &RadioButton_Custom::mouseEnter, this, [=](){设置提示面板信息(UiSelf, V40版本单选框); });
		connect(UiSelf->rdb_V3, &RadioButton_Custom::mouseLeave, this, [=](){设置提示面板信息(UiSelf, 空字符串); });
	QString 日志面板(tr("日志面板——绿色、黄色和红色背景的提示信息分别是流程、警告和错误类型的日志。使用鼠标可单选或多选条目，右键可呼出操作菜单。"));
		connect(UiSelf->lstvw_OutputPanel, &ListView_Custom::mouseEnter, this, [=](){设置提示面板信息(UiSelf, 日志面板); });
		connect(UiSelf->lstvw_OutputPanel, &ListView_Custom::mouseLeave, this, [=](){设置提示面板信息(UiSelf, 空字符串); });
	QString 文件面板(tr("文件面板——展示文件的信息和状态。使用鼠标可单选或多选条目，右键可呼出操作菜单，在窗口内双击可选择并引入文件。"));
		connect(UiSelf->tvw_FilePanel, &TreeView_Custom::mouseEnter, this, [=](){设置提示面板信息(UiSelf, 文件面板); });
		connect(UiSelf->tvw_FilePanel, &TreeView_Custom::mouseLeave, this, [=](){设置提示面板信息(UiSelf, 空字符串); });
}
