QT		+= core gui concurrent winextras

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

# 如果您的代码中包含已弃用的API可能会导致编译失败,为此请取消下行注释.
# DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # 禁用Qt6.0.0之前的所有弃用API.

SOURCES += \
	Class_CustomComponent.cpp \
	Main.cpp \
	Class_AppWindow_MainWindow.cpp

HEADERS += \
	Class_CustomComponent.h \
	Class_AppWindow_MainWindow.h \
	Function_AutoSelectLanguage.hpp \
	Function_FileProcess.hpp \
	Function_OutputLog.hpp

FORMS += \
	Class_AppWindow_MainWindow.ui

TRANSLATIONS += \
	language/DeemoChartConverter_Qt_zh_CN.ts \
	language/DeemoChartConverter_Qt_zh_TW.ts \
	language/DeemoChartConverter_Qt_en_US.ts
CONFIG += embed_translations

# 默认部署规则.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

DISTFILES += \
	language/DeemoChartConverter_Qt_en_US.ts \
	language/DeemoChartConverter_Qt_zh_CN.ts \
	language/DeemoChartConverter_Qt_zh_TW.ts

RESOURCES += \
	ResourcePackage.qrc

RC_FILE = Description.rc
