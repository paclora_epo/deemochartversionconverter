﻿#pragma execution_character_set("UTF-8")
#ifndef CLASS_APPWINDOW_MAINWINDOW_H
#define CLASS_APPWINDOW_MAINWINDOW_H

#include <QMainWindow>
#include <QTimer>
#include <QtWin>

QT_BEGIN_NAMESPACE
namespace Ui { class AppWindow_MainWindow; }
QT_END_NAMESPACE


class AppWindow_MainWindow : public QMainWindow
{
	Q_OBJECT

private:
	//enum 信息类型{流程=1, 警告=2, 错误=3, 调试=4}; // 枚举已迁移至独立文件"Function_OutputLog.hpp".
	enum 文件引入方式{弹窗=1, 拖拽 =2};

public:
	Ui::AppWindow_MainWindow * UiSelf;
	QTimer* 日志列表主动刷新计时器 = new QTimer(this);
	bool 后缀编辑框可自动修改{true};
	QColor 系统主题颜色{QtWin::colorizationColor()}; // https://doc.qt.io/qt-5/qtwin-obsolete.html#colorizationColor
	QString QSS_菜单样式;

public:
	AppWindow_MainWindow(QWidget * parent = nullptr);
	~AppWindow_MainWindow();
	const void 设置提示面板信息(Ui::AppWindow_MainWindow * UiPointer, const QString &信息详情);
	//const void 添加日志面板信息(int 信息类型, QString 信息内容); // 函数已迁移至独立文件"Function_OutputLog.hpp".
	void 面板引入文件(int 文件引入方式);
	void 批量转换文件();
	void 提示面板内容绑定();

private slots:
	void on_btn_LanguageSwitch_clicked();
	void on_lstvw_OutputPanel_customContextMenuRequested(const QPoint &pos);
	void on_lstvw_OutputPanel_customContextMenu_Action_CopySelectedItem();
	void on_lstvw_OutputPanel_customContextMenu_Action_CopyAllItem();
	void on_lstvw_OutputPanel_customContextMenu_Action_ClearSelectedItem();
	void on_lstvw_OutputPanel_customContextMenu_Action_ClearAllItem();
	void on_tvw_FilePanel_customContextMenuRequested(const QPoint &pos);
	void on_tvw_FilePanel_customContextMenu_Action_ImportFile();
	void on_tvw_FilePanel_customContextMenu_Action_ClearSelectedItem();
	void on_tvw_FilePanel_customContextMenu_Action_ClearAllItem();
	void on_ckb_BoundsOut_stateChanged(int arg1);
	void on_ckb_Overlapping_stateChanged(int arg1);
	void on_ckb_TooSmall_stateChanged(int arg1);
	void on_ckb_TooEarly_stateChanged(int arg1);
	void on_ckb_AtypicalValue_stateChanged(int arg1);
	void on_ckb_SkipInvalidSoundsDelay_stateChanged(int arg1);
	void on_rdb_CheckOnly_toggled(bool checked);
	void on_rdb_V1_toggled(bool checked);
	void on_rdb_V2_toggled(bool checked);
	void on_rdb_V3_toggled(bool checked);
	void on_btn_CheckFileVersion_clicked();
	void on_btn_ConvertAllFile_clicked();
	void on_lineEdit_Suffix_textEdited(const QString &arg1);
	void on_ckb_KeepPath_stateChanged(int arg1);
};


#endif // CLASS_APPWINDOW_MAINWINDOW_H
