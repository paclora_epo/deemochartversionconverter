﻿#pragma execution_character_set("UTF-8")
#ifndef CLASS_CUSTOMCOMPONENT_H
#define CLASS_CUSTOMCOMPONENT_H

#include <QWidget>


#include <QPushButton>
class PushButton_Custom : public QPushButton {
	Q_OBJECT
public:
	explicit PushButton_Custom(QWidget * parent = nullptr);
	virtual void enterEvent(QEvent *);
	virtual void leaveEvent(QEvent *);
signals:
	void mouseEnter();
	void mouseLeave();
};

#include <QLineEdit>
class LineEdit_Custom : public QLineEdit {
	Q_OBJECT
public:
	explicit LineEdit_Custom(QWidget * parent = nullptr);
	virtual void enterEvent(QEvent *);
	virtual void leaveEvent(QEvent *);
signals:
	void mouseEnter();
	void mouseLeave();
};

#include <QCheckBox>
class CheckBox_Custom : public QCheckBox {
	Q_OBJECT
public:
	explicit CheckBox_Custom(QWidget * parent = nullptr);
	virtual void enterEvent(QEvent *);
	virtual void leaveEvent(QEvent *);
signals:
	void mouseEnter();
	void mouseLeave();
};

#include <QRadioButton>
class RadioButton_Custom : public QRadioButton {
	Q_OBJECT
public:
	explicit RadioButton_Custom(QWidget * parent = nullptr);
	virtual void enterEvent(QEvent *);
	virtual void leaveEvent(QEvent *);
signals:
	void mouseEnter();
	void mouseLeave();
};

#include <QListView>
class ListView_Custom : public QListView {
	Q_OBJECT
public:
	explicit ListView_Custom(QWidget * parent = nullptr);
	virtual void enterEvent(QEvent *);
	virtual void leaveEvent(QEvent *);
	//virtual void rowsInserted(const QModelIndex &parent, int start, int end);
	QModelIndexList selectedIndexs(); // 中介访问.
signals:
	void mouseEnter();
	void mouseLeave();
};

#include <QTreeView>
#include <QDragEnterEvent>
#include <QDragMoveEvent>
#include <QDropEvent>
#include <QMimeData>
class TreeView_Custom : public QTreeView {
	Q_OBJECT
public:
	QList<QString> 面板引入文件列表中介; // 用完清空.
public:
	explicit TreeView_Custom(QWidget * parent = nullptr);
	virtual void enterEvent(QEvent *);
	virtual void leaveEvent(QEvent *);
	virtual void mouseDoubleClickEvent(QMouseEvent *);
	virtual void dragEnterEvent(QDragEnterEvent * event);
	virtual void dragMoveEvent(QDragMoveEvent *);
	virtual void dropEvent(QDropEvent * event);
	QModelIndexList selectedIndexs(); // 中介访问.
signals:
	void mouseEnter();
	void mouseLeave();
	void mouseDblClick();
	void dropFile();
};


#endif // CLASS_CUSTOMCOMPONENT_H
